//------------------------------------------------------------------------------
// Define the base class consumer
//
// 1. person who buys merchandise, services 
//------------------------------------------------------------------------------
#ifndef GAMESTRADAMUS_CONSUMER_HPP
#define GAMESTRADAMUS_CONSUMER_HPP

#include <boost/cstdint.hpp> 

namespace gamestradamus {

class Consumer {
public:
    typedef boost::uint_fast64_t id_t;

};// end class Consumer

}//end namespace gamestradamus

#endif//GAMESTRADAMUS_CONSUMER_HPP

