//------------------------------------------------------------------------------
// A steam account is a consumer
//------------------------------------------------------------------------------
#ifndef GAMESTRADAMUS_STEAM_ACCOUNT_HPP
#define GAMESTRADAMUS_STEAM_ACCOUNT_HPP

#include <gamestradamus/consumer.hpp> 

namespace gamestradamus {
namespace steam {


class Account : public Consumer {
public:
    //--------------------------------------------------------------------------
    // Faux constructor.
    Account() { }


private:



};// end class Account

}//end namespace steam
}//end namespace gamestradamus

#endif//GAMESTRADAMUS_STEAM_ACCOUNT_HPP

