define([
    "dojo/dom",
    "dojo/query",
    "atemi/util/TableSort",
    "atemi/io/jsonrpc",
    "bootstrap/Tooltip",
    "dojo/domReady!"
], function(dom, query, TableSort, jsonrpc, Tooltip) {

    var init = function() {
        var no_relatives = dom.byId('no-relatives');
        if (no_relatives) {

            var rpc = jsonrpc('/jsonrpc');
            var now = (new Date()).getTime();

            var timer;
            var is_family_tree_updated = function() {
                rpc.request({
                    method: 'predictions.last-family-tree-update',
                    params: []
                }, function(data) {
                    var last_update = Date.parse(data);
                    if (!isNaN(last_update) && last_update > now) {
                        clearInterval(timer);
                        window.location.reload();
                    }
                });
            };
            timer = setInterval(is_family_tree_updated, 2000);
        }

        // Show the account the last time their predictions were updated.
        var last_updated = dom.byId('last-updated');
        var date = Date.parse(last_updated.getAttribute('data-last-updated'));
        if (!isNaN(date)) {
            date = new Date(date);
            var time;
            if (date.getHours() == 0 && date.getMinutes() == 0) {
                time = '12 midnight';
            } else if (date.getHours() == 12 && date.getMinutes() == 0) {
                time = '12 noon';
            } else if (date.getHours() == 0) {
                time = '12:' + ('0' + date.getMinutes()).slice(-2) + ' am';
            } else if (date.getHours() < 12) {
                time = date.getHours() + ':' + ('0' + date.getMinutes()).slice(-2) + ' am';
            } else if (date.getHours() < 13) {
                time = date.getHours() + ':' + ('0' + date.getMinutes()).slice(-2) + ' pm';
            } else {
                time = (date.getHours() - 12) + ':' + ('0' + date.getMinutes()).slice(-2) + ' pm';
            }
            last_updated.innerHTML = 
                ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][date.getMonth()]
                + ' ' + date.getDate() + ', ' + date.getFullYear() + ' at ' + time;
        } else {
            last_updated.innerHTML = 'never';
        }

        query('th.match-score a').tooltip({
            trigger: 'hover',
            placement: 'top',
            html: true,
            title: 'The match score indicates how closely related the player is to your gameplay style. Higher scores indicate closer matches.'
        });

        query('th.similar-games a').tooltip({
            trigger: 'hover',
            placement: 'top',
            html: true,
            title: 'The number of games that are similar between you and the other player.'
        });

        var table = dom.byId('relative_list');
        new TableSort(
            table, [0, 0], [
                TableSort.textContent,
                TableSort.parseInt,
                TableSort.parseFloat
            ]
        );
    };
    return init;
})
