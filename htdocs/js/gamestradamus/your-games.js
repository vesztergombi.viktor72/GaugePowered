define([
    "atemi/io/jsonrpc",
    "atemi/thirdparty/moment",
    "atemi/util/format",
    "atemi/util/TableSort",

    "bootstrap/Tooltip",
    "bootstrap/Modal",

    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/_base/fx",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/keys",
    "dojo/mouse",
    "dojo/on",
    "dojo/query",

    "gamestradamus/analytics/internal",
    "gamestradamus/region",

    "dojo/NodeList-dom"
], function(
    jsonrpc,
    moment,
    format,
    TableSort,

    Tooltip,
    Modal,

    array,
    event,
    fx,
    dom,
    domClass,
    domConstruct,
    keys,
    mouse,
    on,
    query,

    analytics,
    region
) {

    var thumbnail_index = 0;
    var rpc = jsonrpc('/jsonrpc');
    var table = dom.byId('library');
    var now = (new Date()).getTime() - 10000;

    //--------------------------------------------------------------------------
    var fix_firefox_reload_autofill_bug = function() {
        /**
         * Fix a bug with firefox autofilling the wrong checkboxes / dollars / hours on reload.
        **/
        query('input.finished-game', table).forEach(function(item) {
            if (item.getAttribute('checked') == 'checked') {
                item.checked = true;
            } else {
                item.checked = false;
            }
        });

        query('input.dollars-paid', table).forEach(function(item) {
            item.value = item.getAttribute('value');
        });

        query('input.hours-played', table).forEach(function(item) {
            item.value = item.getAttribute('value');
        });
    };

    //--------------------------------------------------------------------------
    var setup_game_thumbnails = function() {
        var games = table.tBodies[0].rows;
        array.some(games, function(game) {
            if (thumbnail_index > 30) {
                return true;
            }

            var img = game.cells[1].children[0].children[0];
            if (img) {
                img.src = img.getAttribute('data-src');
            }

            thumbnail_index++;
        });

        // Update the image links as the individual scrolls down their library page.
        var load_images_to_scroll_pos = function() {
            var t = window.scrollY - 285;
            t = Math.floor(t / 57) + 30;

            while (thumbnail_index < games.length && thumbnail_index < t) {
                var img = games[thumbnail_index].cells[1].children[0].children[0];
                if (img) {
                    img.src = img.getAttribute('data-src');
                }
                thumbnail_index++;
            }
        };
        var signal = on(window, 'scroll', load_images_to_scroll_pos);
        load_images_to_scroll_pos();

        // If the user isn't scrolling the page, slowly load the images in the background.
        var timer = window.setInterval(function() {
            if (thumbnail_index >= games.length) {
                signal.remove();
                return window.clearTimeout(timer);
            }

            var img = games[thumbnail_index].cells[1].children[0].children[0];
            if (img) {
                img.src = img.getAttribute('data-src');
            }
            thumbnail_index++;
        }, 750);
    }; 

    //--------------------------------------------------------------------------
    var extract_hash_parameters = function(hash) {
        // Note: This method varies from dojo/io-query by always returning values
        //       as an array. This makes the rest of the code in this library much
        //       easier to write. 

        var search = window.location.hash.replace('#', '');
            search = search.split('&');
        var params = {};
        array.forEach(search, function(q) {
            q = q.split('=');
            if (!q[0]) {
                return;
            }
            if (!params[q[0]]) {
                params[q[0]] = decodeURIComponent(q[1]).split('_');
            } else {
                array.forEach(decodeURIComponent(q[1]).split('_'), function(value) {
                    params[q[0]].push(value);
                });
            }
        });
        return params;
    };

    //--------------------------------------------------------------------------
    var encode_hash_parameters = function(parameters) {
        var hash = '';
        var first = true;
        for (key in parameters) {
            if (!parameters[key]) {
                continue;
            }

            if (first) {
                hash = key + '=';
                first = false;
            } else {
                hash = hash + '&' + key + '=';
            } 

            hash = hash + encodeURIComponent(parameters[key][0]);
            for (var i = 1; i < parameters[key].length; ++i) {
                hash = hash + '_' + encodeURIComponent(parameters[key][i]);
            }
        }
        if (!hash) {
            // Ensure there is at least one parameter on the hash tag to 
            // avoid the browser snapping the scroll to the top for just #
            hash = 'pre=1';
        }

        return hash;
    };

    //--------------------------------------------------------------------------
    var setup_help_tooltips = function() {
        /**
         * Add some tooltips to the header action links to help guide the user.
        **/

        var container = query('article.your-games')[0];
        query('#your-library-help,#download-csv,#your-ignored-games,#invite-friend,#update-games').tooltip({
            container: container,
            trigger: 'hover',
            placement: 'top',
            html: false
        });

        query('#cost-column-header a').tooltip({
            container: container,
            trigger: 'manual',
            placement: 'top',
            html: true,
            title: '<p>Get started by correcting the prices in this column.</p>'
        });

        on(query('.dismiss', 'message'), 'click', function(evt) {
            event.stop(evt);
             
            fx.animateProperty({
                node: dom.byId('message'),
                properties: {
                    opacity: {start: 1, end: 0},
                    height: {start: 44, end: 0},
                    paddingTop: {start: 20, end: 0}
                },
                onEnd: function() {
                    domClass.add(dom.byId('message'), 'hidden');
                }
            }).play();
        });
    };

    //-------------------------------------------------------------------------- 
    var setup_friends = function() {
        /**
         * Setup the behaviour to invite friends to compare Libraries.
        **/
        var message = dom.byId('message');
        var modal = dom.byId('invitation-modal');
        var email_input = query('input.email', modal)[0];
        query(modal).modal({show: false});

        on(query('.friends >a,#invite-friend'), 'click', function(evt) {
            event.stop(evt);
            query(modal).modal('show');
        });

        var select = query('.friends select')[0];
        if (select) {
            select.selectedIndex = 0;
            on(select, 'change', function(evt) {
                if (select.selectedIndex > 1) {
                    window.location = '/member/' + select[select.selectedIndex].value + '/games/';
                }
            });
        }

        on(query('form', modal), 'submit', function(evt) {
            event.stop(evt);

            var email = email_input.value;
                email = email.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
            if (!email) {
                return;
            }

            analytics.trackEvent('Update Library', 'Invite Friend');
            rpc.request({
                method: 'useraccountinvitation.create',
                params: ['', {email: email}]

            }, function(data) {
                email_input.value = '';
                query(modal).modal('hide');

                domClass.remove(message, 'hidden');
                query('p', message)[0].innerHTML = 
                    'Your invitation has been sent! Once your friend accepts the invitation, you will be able to view their Library.'; 
                fx.animateProperty({
                    node: dom.byId('message'),
                    properties: {
                        height: {start: 0, end: 44},
                        opacity: {start: 0, end: 1},
                        paddingTop: {start: 0, end: 20}
                    }
                }).play();

            }, function(error) {
                query(email_input).tooltip('destroy');
                query(email_input).tooltip({
                    trigger: 'manual',
                    placement: 'top',
                    html: false,
                    title: email + ' is not a valid e-mail address'
                });
                query(email_input).tooltip('show');
            });
        });

        on(email_input, 'focus', function(evt) {
            query(email_input).tooltip('destroy');
        });

        on(modal, 'hide', function(evt) {
            // Hide any error tooltips when the modal gets hidden.
            query(email_input).tooltip('destroy');
        });
    };

    //--------------------------------------------------------------------------
    var _fix_table_stripes = function() {
        var games = table.tBodies[0].rows;
        var index = 1, hours_played, dollars_paid, img;
        for (var i = 0, len = games.length, tr; i < len; ++i) {
            tr = games[i];

            if (domClass.contains(tr, 'blur') || domClass.contains(tr, 'ignore')) {
                continue;
            }

            // Fix the tab index ordering for the rows.
            // dollars_paid = query('input.dollars-paid', tr)[0];
            dollars_paid = tr.cells[2].children[0];
            dollars_paid.tabIndex = index;

            // hours_played = query('input.hours-played', tr)[0];
            hours_played = tr.cells[3].children[0];
            hours_played.tabIndex = index + len;

            // Fix the positioning column.
            tr.cells[0].innerHTML = '<div>' + index + '</div>';
            
            // Fix the table striping.
            if (index % 2) {
                domClass.add(tr, 'odd');
            } else {
                domClass.remove(tr, 'odd');
            }

            img = tr.cells[1].children[0].children[0];
            if (img) {
                img.src = img.getAttribute('data-src');
            }

            index++;
        }

        // Disable the thumbnail scroll loading.
        thumbnail_index = games.length;
    };

    //--------------------------------------------------------------------------
    var setup_last_game_update = function() {
        /**
         * Show the account the last time their games were updated.
        **/

        var last_updated = dom.byId('last-updated');
        function last_updated_format(m) {
            return m.format("MMM D, YYYY") + " at " + m.format("h:mm a");
        }
        
        var date = Date.parse(last_updated.getAttribute('data-last-updated'));
        if (!isNaN(date)) {
            date = moment(new Date(date));
            last_updated.innerHTML = last_updated_format(date);
        } else {
            last_updated.innerHTML = 'never';
        }

        var timer;
        var are_games_updated = function() {
            rpc.request({
                method: 'predictions.last-game-library-update',
                params: []
            }, function(data) {
                var last_update = Date.parse(data.last_updated);
                if (!isNaN(last_update) && last_update > now) {
                    clearInterval(timer);

                    // Show the div again, and hide the loader
                    domClass.remove(dom.byId('last-updated-wrapper'), 'hidden');
                    domClass.add(dom.byId('update-games-loader'), 'hidden');
                    dom.byId("last-updated-wrapper").innerHTML = "Steam update has finished. <a id=\"reload\">(reload page)</a>";

                    on(dom.byId("reload"), "click", function() {
                        window.location.reload();
                    });
                }
            });
        };

        // If there is a update games call running when the page is loaded, 
        // wait until the operation is finished and reload the page.
        var update_games_loader = dom.byId('update-games-loader');
        if (!domClass.contains(update_games_loader, 'hidden')) {
            timer = setInterval(are_games_updated, 1500);
        }

        // Attach the functionality to the update from steam link.
        var update_games = dom.byId('update-games');
        if (update_games) {
            on(update_games, 'click', function(evt) {
                event.stop(evt);

                domClass.add(dom.byId('last-updated-wrapper'), 'hidden');
                domClass.remove(dom.byId('update-games-loader'), 'hidden');
                domClass.add(dom.byId('update-games'), 'hidden');

                analytics.trackEvent('Update Library', 'Sync With Steam');
                rpc.request({
                    method: 'predictions.update-games',
                    params: []
                }, function(data) {
                    timer = setInterval(are_games_updated, 1500);
                });
            }); 
        }
    };

    //--------------------------------------------------------------------------
    var tabulate = function() {
        /**
         * Tabulate the values displayed in the table.
        **/
        var hours_played = 0;
        var dollars_paid = 0;
        var steam_price = 0;
        var rating = 0;

        var counts = {
            total: 0,
            rating: 0,
            cost: 0,
            finished: 0
        };
        var allow_instructions = true;

        var games = table.tBodies[0].rows;
        array.forEach(games, function(item) {
            if (domClass.contains(item, 'ignore')) {
                return;
            }
            if (domClass.contains(item, 'blur')) {
                // If we are filtering anything out, don't show the initial instructions.
                allow_instructions = false;
                return;
            }

            var value, input;

            counts.total++;
            // query('input.finished-game', item.cells[6])[0];
            input = item.cells[6].children[0].children[0];
            if (input.checked) {
                counts.finished++;
            }
            // query('input.dollars-paid', item.cells[2])[0];
            input = item.cells[2].children[0];
            if (!domClass.contains(input, 'uncorrected')) {
                counts.cost++;
            }
            value = region['parse-dollars'](input.value);
            if (!isNaN(value)) {
                dollars_paid = dollars_paid + value;
            }
            value = region['parse-dollars'](input.getAttribute('data-steam-price'));
            if (!isNaN(value)) {
                steam_price = steam_price + value;
            }
            // query('input.hours-played', item.cells[3])[0];
            input = item.cells[3].children[0];
            value = region['parse-hours'](input.value);
            if (!isNaN(value)) {
                hours_played = hours_played + value;
            }
            // query('div.rating', item.cells[5])[0]
            input = item.cells[5].children[0];
            value = parseInt(input.getAttribute('data-rating'));
            if (value > 0) {
                rating = rating + value;
                counts.rating++;
            }
        });

        var item = query('tfoot tr', table)[0];
        if (region['symbol-position'] == 'before') {
            item.cells[2].innerHTML =
                region['symbol'] + 
                region['format-dollars'](dollars_paid);
        } else {
            item.cells[2].innerHTML = 
                region['format-dollars'](dollars_paid) +
                region['symbol']; 
        }

        item.cells[3].innerHTML = region['format-hours'](hours_played);
        if (dollars_paid > 0) {
            item.cells[4].innerHTML = region['format-cost-per-hour'](dollars_paid / hours_played);
        } else {
            item.cells[4].innerHTML = '&mdash;';
        }
        if (counts.rating > 0) {
            item.cells[5].innerHTML = region['format-rating'](rating / counts.rating);
        } else {
            item.cells[5].innerHTML = '&mdash;';
        }

        // Update the global statistics.
        var stat;
        stat = query('div.average-cost-per-hour >.stat', 'aggregates')[0];
        if (hours_played > 0) {

            if (Math.round((dollars_paid * 100) / hours_played) >= 100) {
                if (region['symbol-position'] == 'before') {
                    stat.innerHTML = 
                        '<span class="unit">' + region['symbol'] + '</span>' + 
                        region['format-cost-per-hour'](dollars_paid / hours_played);
                } else {
                    stat.innerHTML = 
                        region['format-cost-per-hour'](dollars_paid / hours_played) +
                        '<span class="unit">' + region['symbol'] + '</span>';
                }
            } else {
                stat.innerHTML = ((dollars_paid / hours_played) * 100).toFixed(0) +
                    '<span class="unit"> ' + region['fraction-symbol'] + '</span>';
            }

            stat.setAttribute('data-stat', (dollars_paid / hours_played).toFixed(4));
        } else {
            stat.innerHTML = '0<span class="unit">' + region['fraction-symbol'] + '</span>';
            stat.setAttribute('data-stat', '0.00');
        }

        stat = query('div.total-cost >.stat', 'aggregates')[0];
        stat.innerHTML = region['format-dollars'](dollars_paid);
        var title = 'You\'ve saved <strong>';
        if (region['symbol-position'] == 'before') {
            title += region['symbol'] + region['format-dollars'](steam_price - dollars_paid);
        } else {
            title += region['format-dollars'](steam_price - dollars_paid) + region['symbol'];
        }
        title += '</strong>';
        query('div.total-cost', 'aggregates').tooltip('destroy');
        query('div.total-cost', 'aggregates').tooltip({
            trigger: 'hover',
            position: 'top',
            html: true,
            title: title
        });

        stat = query('div.total-playtime >.stat', 'aggregates')[0];
        stat.innerHTML = region['format-hours'](hours_played);

        stat = query('div.average-cost >.stat', 'aggregates')[0];
        if (counts.total > 0) {
            stat.innerHTML = region['format-dollars'](dollars_paid / counts.total);
        } else {
            stat.innerHTML = '0.00';
        }

        stat = query('div.average-playtime >.stat', 'aggregates')[0];
        if (counts.total > 0) {
            stat.innerHTML = region['format-hours'](hours_played / counts.total);
        } else {
            stat.innerHTML = '0.0';
        }

        // Update the Cost completion progress bar.
        var cost_percentage = ((counts.cost / counts.total) * 100);
        query('span', 'progress-bar').forEach(function(span) {
            if (cost_percentage > 0) {
                span.innerHTML = '<strong>' + cost_percentage.toFixed(1) + 
                    '%</strong> (' + counts.cost + ' of ' + counts.total + ' games) corrected';
            } else {
                span.innerHTML = '';
            }
        });

        var progress = query('.progress', 'progress-bar')[0];
        progress.style.width = cost_percentage.toFixed(1) + '%';
        if (counts.cost == counts.total) {
            domClass.add(dom.byId('progress-bar'), 'complete');
        } else {
            domClass.remove(dom.byId('progress-bar'), 'complete');
        }

        // Show the help tooltip when a profile has zero corrected costs.
        if (counts.cost < 1 && counts.total > 0 && allow_instructions) {
            query('a', 'cost-column-header').tooltip('show');
        } else {
            query('a', 'cost-column-header').tooltip('hide');
        }
    };

    //--------------------------------------------------------------------------
    var setup_table_sorting = function() {
        /**
         * Setup the table sorting.
        **/
        var cost_per_hour_anchor = query('>a', 'cost-per-hour-column-header')[0];
        var name_anchor = query('>a', 'name-column-header')[0];
        var cost_anchor = query('>a', 'cost-column-header')[0];
        var hours_anchor = query('>a', 'hours-column-header')[0];
        var rating_anchor = query('>a', 'rating-column-header')[0];
        new TableSort(
            table, [0, 0], [
                TableSort.textContent,
                TableSort.textContent,
                function(td) {
                    if (td) {
                        var dollars_paid = query('input.dollars-paid', td)[0];
                        return region['parse-dollars'](dollars_paid.value); 
                    } else {
                        return 0.00;
                    }
                },
                function(td) {
                    if (td) {
                        var hours_played = query('input.hours-played', td)[0];
                        return region['parse-hours'](hours_played.value); 
                    } else {
                        return 0.00;
                    }
                },
                function(td) {
                    if (td) {
                        var order = domClass.contains(
                            cost_per_hour_anchor, 'sort-descending'
                        );
                        var value = region['parse-hours'](td.textContent);
                        if (isNaN(value)) {
                            if (!order) {
                                return Infinity;
                            } else {
                                return -Infinity;
                            }
                        }
                        return value;
                    } else {
                        return 0;
                    }
                },
                function(td) {
                    if (td) {
                        var rating = query('div.rating', td)[0];
                        var cost_per_hour = query('span.cost-per-hour', td.parentNode)[0];
                            cost_per_hour = region['parse-hours'](cost_per_hour.textContent);
                        if (isNaN(cost_per_hour)) {
                            cost_per_hour = Infinity;
                        } 
                        return [
                            parseInt(rating.getAttribute('data-rating')),
                            -cost_per_hour
                        ];
                    } else {
                        return [0, 0];
                    }
                }
            ],
            function(data) {
                var log_sorting_event = function(anchor, action) {
                    if (domClass.contains(anchor, 'sort-ascending')) {
                        analytics.trackEvent('Sorting', action, 'Ascending');
                    }
                    if (domClass.contains(anchor, 'sort-descending')) {
                        analytics.trackEvent('Sorting', action, 'Descending');
                    }
                }

                log_sorting_event(name_anchor, 'Game Name');
                log_sorting_event(cost_anchor, 'Cost');
                log_sorting_event(hours_anchor, 'Hours');
                log_sorting_event(rating_anchor, 'Rating');
                log_sorting_event(cost_per_hour_anchor, 'Value');

                // Make sure that everything is striped properly.
                _fix_table_stripes();

                // Update the window.location.hash to include the table sorting.
                var sort, params;
                array.some([
                    ['name', 'ASC'],
                    ['name', 'DESC'],
                    ['cost', 'ASC'],
                    ['cost', 'DESC'],
                    ['hours', 'ASC'],
                    ['hours', 'DESC'],
                    ['rating', 'ASC'],
                    ['rating', 'DESC'],
                    ['cost-per-hour', 'ASC'],
                    ['cost-per-hour', 'DESC']
                ], function(item) {
                    if (item[1] == 'ASC') {
                        if (domClass.contains(query('>a', item[0] + '-column-header')[0], 'sort-ascending')) {
                            sort = item;
                            return true;
                        }
                    } else if (item[1] == 'DESC') {
                        if (domClass.contains(query('>a', item[0] + '-column-header')[0], 'sort-descending')) {
                            sort = item;
                            return true;
                        }
                    }
                });

                params = extract_hash_parameters(window.location.hash);
                params['sort'] = sort;
                window.location.hash = encode_hash_parameters(params);
            }
        ); 
    };

    //--------------------------------------------------------------------------
    var _filter_library = function() {
        /**
         * Provide the functionality for the filter interface.
        **/

        // Which filters are current in effect?
        var tag_filter_list = [];
        var untag_filter_list = [];
        var genre_filter_list = [];
        var finished_filter_list = [];
        var hours_filter_list = [];
        var cost_filter_list = [];
        var apply_rating_filter = false;
        var rating_filter_map = {};
        var applied_filters = false;
        query('.filter-list >.filter', 'filter-list').forEach(function(filter) {
            if (domClass.contains(filter, 'hidden')) {
                return;
            }
            applied_filters = true;
            
            if (filter.getAttribute('data-tag-name')) {
                tag_filter_list.push(filter.getAttribute('data-tag-name'));

            } else if (filter.getAttribute('data-untag-name')) {
                untag_filter_list.push(
                    filter.getAttribute('data-untag-name').split(' / ')[0]
                );

            } else if (filter.getAttribute('data-genre')) {
                genre_filter_list.push(filter.getAttribute('data-genre'));

            } else if (filter.getAttribute('data-rating')) {
                rating_filter_map[filter.getAttribute('data-rating')] = true;
                apply_rating_filter = true;

            } else if (filter.getAttribute('data-finished')) {
                finished_filter_list.push(filter.getAttribute('data-finished'));

            } else if (filter.getAttribute('data-cost')) {
                cost_filter_list.push(filter.getAttribute('data-cost'));   

            } else if (filter.getAttribute('data-hours')) {
                hours_filter_list.push(filter.getAttribute('data-hours'));
            }
        }); 

        // Update window.location.hash
        var params = extract_hash_parameters(window.location.hash);

        var composite_tag_filter_list = [];
        array.forEach(tag_filter_list, function(filter) {
            composite_tag_filter_list.push(filter);
        });
        array.forEach(untag_filter_list, function(filter) {
            composite_tag_filter_list.push(filter + ' / No Tag');
        });
        if (composite_tag_filter_list.length) {
            params['tag'] = composite_tag_filter_list;
        } else {
            params['tag'] = undefined;
        }

        if (genre_filter_list.length) {
            params['genre'] = genre_filter_list;
        } else {
            params['genre'] = undefined;
        }

        if (cost_filter_list.length) {
            params['cost'] = cost_filter_list;
        } else {
            params['cost'] = undefined;
        }

        if (hours_filter_list.length) {
            params['hours'] = hours_filter_list;
        } else {
            params['hours'] = undefined;
        }

        if (finished_filter_list.length) {
            params['finished'] = finished_filter_list;
        } else {
            params['finished'] = undefined;
        }

        if (apply_rating_filter) {
            var enjoyment = [];
            for (key in rating_filter_map) {
                enjoyment.push([key]);
            }
            params['enjoyment'] = enjoyment;
        } else {
            params['enjoyment'] = undefined;
        }
        window.location.hash = encode_hash_parameters(params);

        // Open and close the filter interface.
        var rollup = query('.rollup', 'filter-list')[0];
        if (applied_filters && !domClass.contains(rollup, 'open')) {
            domClass.add(rollup, 'open');
            fx.animateProperty({
                node: rollup,
                properties: {
                    height: {start: 0, end: 34},
                    paddingBottom: {start: 0, end: 10}
                },
                onEnd: function() {
                    rollup.style.height = 'auto';
                }
            }).play(); 
        } else if(!applied_filters && domClass.contains(rollup, 'open')) {
            fx.animateProperty({
                node: rollup,
                properties: {
                    height: {start: rollup.clientHeight - 10, end: 0},
                    paddingBottom: {start: 10, end: 0}
                },
                onEnd: function() {
                    domClass.remove(rollup, 'open');
                }
            }).play(); 
        }

        var finished_map = {
            "Yes": true,
            "No": false
        };
        var hours_map = {
            "0-1": [0, 1],
            "1-8": [1, 8],
            "8-20": [8, 20],
            "20-40": [20, 40],
            "40-100000": [40, 100000]
        };
        var cost_map = {
            "0-5": [0, 5],
            "5-10": [5, 10],
            "10-20": [10, 20],
            "20-40": [20, 40],
            "40-10000": [40, 10000],

            "0-150": [0, 150],
            "150-300": [150, 300],
            "300-500": [300, 500],
            "500-700": [500, 700],
            "700-10000": [700, 10000]
        };

        var games = table.tBodies[0].rows;
        var count = games.length;
        var tr, group_map, tag_map, genres, filtered;
        for (var i = 0, len = games.length; i < len; ++i) {
            tr = games[i];
            domClass.remove(tr, 'blur');
            filtered = false;

            if (apply_rating_filter) {
                if (!rating_filter_map[query('div.rating', tr.cells[5])[0].getAttribute('data-rating')]) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }
            }

            if (finished_filter_list.length) {
                if (query('input.finished-game', tr.cells[6])[0].checked != finished_map[finished_filter_list[0]]) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }
            }

            if (cost_filter_list.length) {
                var cost = region['parse-dollars'](query('input.dollars-paid', tr.cells[2])[0].value);
                if (isNaN(cost)) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }

                filtered = true;
                for (var j = 0, l = cost_filter_list.length; j < l; ++j) {
                    if (
                        (cost_map[cost_filter_list[j]][0] <= cost) &&
                        (cost_map[cost_filter_list[j]][1] > cost)
                    ) {
                        filtered = false;
                        break;
                    }
                } 

                if (filtered) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }
            }

            if (hours_filter_list.length && !filtered) {
                var hours = region['parse-hours'](query('input.hours-played', tr.cells[3])[0].value);
                if (isNaN(hours)) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }

                filtered = true;
                for (var j = 0, l = hours_filter_list.length; j < l; ++j) {
                    if (
                        (hours_map[hours_filter_list[j]][0] <= hours) &&
                        (hours_map[hours_filter_list[j]][1] > hours)
                    ) {
                        filtered = false;
                        break;
                    }
                }

                if (filtered) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }
            }

            if (genre_filter_list.length && !filtered) {
                genres = query('.genre', tr.cells[1])[0];
                if (!genres) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;

                } else {
                    for (var j = 0, l = genre_filter_list.length; j < l; ++j) {
                        if (genres.innerHTML.indexOf(genre_filter_list[j]) < 0) {
                            domClass.add(tr, 'blur');
                            count--;
                            filtered = true;
                            break;
                        }
                    }
                    if (filtered) {
                        continue;
                    }
                }
            }

            if (untag_filter_list.length && !filtered) {
                group_map = {};
                query('.tag', tr.cells[1]).forEach(function(tag) {
                    var pieces = tag.getAttribute('data-tag-name').split(' / ');
                    if (pieces.length > 1) {
                        group_map[pieces[0]] = true;
                    }
                });
                for (var j = 0, l = untag_filter_list.length; j < l; ++j) {
                    if (group_map[untag_filter_list[j]]) {
                        domClass.add(tr, 'blur');
                        count--;
                        filtered = true;
                        break;
                    }
                }
                if (filtered) {
                    continue;
                }
            }

            if (tag_filter_list.length && !filtered) {
                tag_map = {};
                query('.tag', tr.cells[1]).forEach(function(tag) {
                    tag_map[tag.getAttribute('data-tag-name')] = true;
                });
                for (var j = 0, l = tag_filter_list.length; j < l; ++j) {
                    if (!tag_map[tag_filter_list[j]]) {
                        domClass.add(tr, 'blur');
                        count--;
                        break;
                    }
                }
            }
        }

        if (!count) {
            domClass.add(table.tBodies[0], 'hidden');
            domClass.remove(table.tBodies[1], 'hidden');
        } else {
            domClass.remove(table.tBodies[0], 'hidden');
            domClass.add(table.tBodies[1], 'hidden');
        }
    }; 

    //--------------------------------------------------------------------------
    var setup_premium_nagging = function() {
        var modal = dom.byId('premium-modal');
        on(query('a.premium-support'), 'click', function(evt) {
            event.stop(evt);
            var anchor = evt.currentTarget;

            query(modal).modal('show');  
        }); 
    };

    //--------------------------------------------------------------------------
    var setup_filtering = function() {

        var remove_filter_click_handler = function(evt) {
            /**
             * Invoked when a filter is removed.
            **/
            event.stop(evt);

            domClass.add(evt.currentTarget.parentNode, 'hidden');

            _filter_library();
            tabulate();
            _fix_table_stripes();
        };
        on(query('.filter-list >.filter >a', 'filter-list'), 'click', remove_filter_click_handler);

        var filter_click_handler = function(evt) {
            /**
             * Clicking on a filter should filter the library display.
            **/
            event.stop(evt);

            var filter_list = query('.filter-list >.filter', 'filter-list');

            var tag_name = evt.currentTarget.getAttribute('data-tag-name');
            if (tag_name) {
                analytics.trackEvent('Filtering', 'Tag');
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-tag-name') == tag_name) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            } 

            var untag_name = evt.currentTarget.getAttribute('data-untag-name');
            if (untag_name) {
                analytics.trackEvent('Filtering', 'Tag');
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-untag-name') == untag_name) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            }

            var genre = evt.currentTarget.getAttribute('data-genre');
            if (genre) {
                analytics.trackEvent('Filtering', 'Genre', genre);
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-genre') == genre) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            }

            var rating = evt.currentTarget.getAttribute('data-rating');
            if (rating) {
                analytics.trackEvent('Filtering', 'Rating', rating);
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-rating') == rating) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            }

            var is_finished = evt.currentTarget.getAttribute('data-finished');
            if (is_finished) {
                analytics.trackEvent('Filtering', 'Finished', is_finished);

                // Special case: Only one of Yes or No are allowed for the Finished filter.
                filter_list.forEach(function(filter) {
                    if (filter.getAttribute('data-finished')) {
                        if (filter.getAttribute('data-finished') == is_finished) {
                            domClass.remove(filter, 'hidden');
                        } else {
                            domClass.add(filter, 'hidden');
                        }
                    }
                });
            }

            var hours = evt.currentTarget.getAttribute('data-hours');
            if (hours) {
                analytics.trackEvent('Filtering', 'Duration', hours);

                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-hours') == hours) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                }); 
            } 

            var cost = evt.currentTarget.getAttribute('data-cost');
            if (cost) {
                analytics.trackEvent('Filtering', 'Cost', cost);

                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-cost') == cost) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            }

            _filter_library();
            tabulate();
            _fix_table_stripes();
        };
        on(query('.tag-list .tag', table), 'click', filter_click_handler);
        on(query('.dropdown a', 'filter-column-header'), 'click', filter_click_handler);

        /* Calculate a color index for a tag based on its name */
        var color_hash = function(s) {
            var h = 0;
            for (var i = 0, len = s.length; i < len; ++i) {
                h += (s.charCodeAt(i) ^ 2);
            }
            return h % 8;
        };

        /* Hook up the edit tag buttons */
        var modal = dom.byId('tag-modal');
        on(query('a.edit-tags'), 'click', function(evt) {
            event.stop(evt);
            var anchor = evt.currentTarget;

            // The modal needs to know which game its editing.
            modal.setAttribute(
                'data-user-account-game-id',
                anchor.getAttribute('data-user-account-game-id')
            );

            // Update the checkboxes so that they represent the games tags.
            var input_tag_list = query('input.tag', modal);
            input_tag_list.forEach(function(input) {
                input.checked = false;
            });
            var game_tag_list = anchor.getAttribute('data-tags').split('|'); 
            array.forEach(game_tag_list, function(tag_name) {
                input_tag_list.some(function(input) {
                    if (input.getAttribute('data-tag-name') == tag_name) {
                        input.checked = true;
                        return true;
                    }
                });
            });
            query('h4 span', modal)[0].innerHTML = anchor.getAttribute('data-game-name');

            query(modal).modal('show');
        });

        /* Save the new list of tags when the user clicks "Save Tags" */
        on(query('button.primary', modal), 'click', function(evt) {
            event.stop(evt);

            analytics.trackEvent('Update Library', 'Tag');

            var tags = null;
            var groups = [];
            var input_tag_list = query('input.tag', modal);
            input_tag_list.forEach(function(input) {
                if (input.checked) {
                    if (tags) {
                        tags += '|' + input.getAttribute('data-tag-name');
                    } else {
                        tags = input.getAttribute('data-tag-name');
                    }
                    groups.push(input.getAttribute('data-tag-group'));
                }
            });

            var id = modal.getAttribute('data-user-account-game-id');
            rpc.request({
                method: 'useraccountgame.update',
                params: ['id=' + id + '&select(id,tags)', {tags: tags}]
            }, function(data) {
                // update the displayed tags for this game.
                var games = table.tBodies[0].rows;
                for (var i = 0, len = games.length; i < len; ++i) {
                    var tr = games[i];
                    if (tr.getAttribute('data-user-account-game-id') != id) {
                        continue;
                    }
                    var edit_tags_anchor = query('.edit-tags', tr)[0];
                        edit_tags_anchor.setAttribute('data-tags', data[0].tags);

                    var tag_list_container = query('.tag-list', tr)[0];
                    domConstruct.empty(tag_list_container);
                    if (data[0].tags) {
                        data[0].tags = data[0].tags.split('|');

                        array.forEach(data[0].tags, function(qualified_tag_name, index) {
                            var pieces = qualified_tag_name.split(' / ');
                            if (pieces.length < 2) {
                                pieces.unshift('');
                            } 

                            var node = domConstruct.create('a', {
                                className: 'tag color-' + color_hash(pieces[0]),
                                innerHTML: pieces[1]
                            });
                            node.setAttribute('data-tag-name', qualified_tag_name);
                            node.setAttribute('title', qualified_tag_name);
                            domConstruct.place(node, tag_list_container, 'last');
                            on(node, 'click', filter_click_handler);
                        });
                    }
                    break; 
                }

                query(modal).modal('hide');
            });
        });

        // Handle adding new tags to the users list.
        on(query('form', modal), 'submit', function(evt) {
            event.stop(evt);

            var dropdown_options = query('.dropdown .options', 'filter-column-header')[0];
            var filter_list_container = query('.filter-list', 'filter-list')[0];
            var input_tag_list_container = query('.tag-list', modal)[0];
            var input_tag_list = query('input.tag', input_tag_list_container);
            var new_tag = query('input.new-tag', modal)[0];
            var new_tag_name = new_tag.value.replace(/^\s+|\s+$/g, '');

            if (!new_tag_name) {
                // We shouldn't create empty tags.
                return;
            }
            var at_least_one = input_tag_list.some(function(input) {
                if (input.getAttribute('data-tag-name') == new_tag_name) {
                    return true;
                } else {
                    return false;
                }
            });
            if (at_least_one) {
                // We shouldn't create duplicate tags.
                query(new_tag).tooltip('destroy');
                query(new_tag).tooltip({
                    html: false,
                    placement: 'left',
                    title: 'You already have a tag named "' + new_tag_name + '". Please choose another name.',
                    trigger: 'manual'
                });
                query(new_tag).tooltip('show');
                return;
            }

            analytics.trackEvent('Tags', 'Create');
            rpc.request({
                method: 'useraccounttag.create',
                params: ['', {name: new_tag_name}]
            }, function(data) {
                query(new_tag, modal).tooltip('destroy'); 

                /* Add the new tag to the modal list. */
                var node = domConstruct.create('div', {
                    innerHTML: '<label><input class="tag" ' +
                        'type="checkbox" ' +
                        'data-tag-name="' + data[0].name + '" ' +
                        'data-tag-group="">' + data[0].name +
                        '</label>'
                });
                domClass.add(node, 'field');
                domConstruct.place(node, input_tag_list_container, 'last');

                /* We need a new filter for the tag in the header. */
                var node = domConstruct.create('div', {
                    innerHTML: '<span>' + data[0].name + '</span>' + 
                        '<a title="Stop filtering by \'' + data[0].name + '">×</a>',
                    className: 'filter hidden color-0'
                });
                node.setAttribute('data-tag-name', data[0].name);
                domConstruct.place(node, filter_list_container, 'last');
                on(query('a', node), 'click', remove_filter_click_handler);

                // We need a new filter entry on the dropdown menu.
                var node = domConstruct.create('a', {
                    innerHTML: data[0].name
                });
                node.setAttribute('data-tag-name', data[0].name);
                domConstruct.place(node, dropdown_options, 'last')
                on(node, 'click', filter_click_handler);

                // Clear the input field to allow for rapid adding of multiple tags.
                new_tag.value = '';

            }, function(data) {
                query(new_tag, modal).tooltip('destroy');

                var field = 'name: ';
                if (data.error.message.slice(0, field.length) == field) {
                    query(new_tag, modal).tooltip({
                        html: false,
                        placement: 'left',
                        title: data.error.message.slice(
                            field.length,
                            data.error.message.length
                        ),
                        trigger: 'manual'
                    });
                    query(new_tag, modal).tooltip('show');
                }
            });
        });

        on(query('input.new-tag', modal), 'focus', function(evt) {
            query('input.new-tag', modal).tooltip('destroy'); 
        });
        on(modal, 'hide', function(evt) {
            query('input.new-tag', modal).tooltip('destroy');
        });

        query(modal).modal({show: false});
    };

    //--------------------------------------------------------------------------
    var pre_filter_library = function() {
        /** 
         * Filter the Library down initially based on GET parameters given in the URL.
        **/
        var apply_filtering = false;
        var apply_sorting = false;
        var filters = query('.filter-list >.filter', 'filter-list');
        var params = extract_hash_parameters(window.location.hash);

        if (params['tag']) {
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['tag'], filter.getAttribute('data-tag-name')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
                if (array.indexOf(params['tag'], filter.getAttribute('data-untag-name')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }

        if (params['genre']) {
            // Pre-filter the Library by genre.
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['genre'], filter.getAttribute('data-genre')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }

        if (params['hours']) {
            // Pre-filter the Library by hours.
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['hours'], filter.getAttribute('data-hours')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            }); 
        }

        if (params['cost']) {
            // Pre-filter the Library by cost.
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['cost'], filter.getAttribute('data-cost')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }

        if (params['enjoyment']) {
            // Pre-filter the Library by enjoyment rating.
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['enjoyment'], filter.getAttribute('data-rating')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }

        if (params['finished']) {
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['finished'], filter.getAttribute('data-finished')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }
        if (apply_filtering) {
            // We don't tabulate here because it is done later in init();
            _filter_library();
        }

        if (params['sort']) {
            var column = dom.byId(params['sort'][0] + '-column-header');
            if (column) {
                var anchor = query('>a', column)[0];
                var direction = params['sort'][1];

                if (direction == 'ASC') {
                    domClass.add(anchor, 'sort-descending');
                    domClass.remove(anchor, 'sort-ascending');
                } else {
                    domClass.add(anchor, 'sort-ascending');
                    domClass.remove(anchor, 'sort-descending'); 
                }
                apply_sorting = true;
                on.emit(
                    anchor, 'click', {
                        bubbles: true,
                        cancelable: true 
                    }
                );
            }
        }

        if (apply_filtering && !apply_sorting) {
            // If we don't set an initial table sort, then we need to fix the table stripes.
            _fix_table_stripes();
        }
    };

    //--------------------------------------------------------------------------
    var setup_data_collection = function() {
        /**
         * Setup the interface components for collecting data from the user.
         * Cost / Hours / Ratings / Finished interface.
        **/
        var games = table.tBodies[0].rows;
        var container = query('article.your-games')[0];
        array.forEach(games, function(game) {
            var id = parseInt(game.getAttribute('data-user-account-game-id'));

            var hours_played = game.cells[3].children[0];
            var dollars_paid = game.cells[2].children[0];
            var cost_per_hour = game.cells[4].children[0];

            // Update the hours played. 
            on(hours_played, 'change', function(evt) {
                var steam_hours_played = region['parse-hours'](hours_played.getAttribute('data-steam-hours-played'));
                var hours_played_value = region['parse-hours'](hours_played.value);
                var dollars_paid_value = region['parse-dollars'](dollars_paid.value);
                if (isNaN(steam_hours_played) || isNaN(hours_played_value) || isNaN(dollars_paid_value)) {
                    return;
                }
                var custom_hours_played = hours_played_value - steam_hours_played;

                var cost_per_hour_value = 0.00;
                if (hours_played_value != 0) {
                    cost_per_hour_value = (dollars_paid_value / hours_played_value);
                    if (cost_per_hour_value > dollars_paid_value) {
                        cost_per_hour_value = dollars_paid_value;
                    }
                }

                analytics.trackEvent('Update Library', 'Hours Played');
                rpc.request({
                    method: 'useraccountgame.update',
                    params: [
                        'id=' + id, {
                            custom_hours_played: custom_hours_played.toFixed(2),
                            cost_per_hour: cost_per_hour_value.toFixed(2)
                        }
                    ]
                }, function(data) {
                    if (dollars_paid_value && hours_played_value) {
                        cost_per_hour.innerHTML = region['format-cost-per-hour'](data[0].cost_per_hour); 
                    } else {
                        cost_per_hour.innerHTML = '&mdash;';
                    }

                    hours_played.blur();
                    if (hours_played_value != steam_hours_played) {
                        domClass.add(hours_played, 'altered');
                    } else {
                        domClass.remove(hours_played, 'altered');
                    }

                    tabulate();
                }); 
            });

            // Tooltips
            on(hours_played, 'focus', function(evt) {
                // Tooltip showing the hour breakdown.
                query(hours_played).tooltip({
                    container: container,
                    trigger: 'manual',
                    placement: 'right',
                    html: true,
                    title: '<strong>Steam:</strong> ' + hours_played.getAttribute('data-steam-hours-played') + ' hrs'
                }).tooltip('show');
            });
            on(hours_played, 'blur', function(evt) {
                query(hours_played).tooltip('destroy');
            });

            var dollars_paid_tooltip_title = function() {
                var string = '';
                if (domClass.contains(dollars_paid, 'uncorrected')) { 
                    
                    string = 'The regular price is <a class="price" data-value="' + dollars_paid.getAttribute('data-steam-price') + '">';
                    if (region['symbol-position'] == 'before') {
                        string += region['symbol'] + dollars_paid.getAttribute('data-steam-price');
                    } else {
                        string += dollars_paid.getAttribute('data-steam-price') + region['symbol'];
                    }
                    string += '</a>. ';

                    if (parseInt(dollars_paid.getAttribute('data-on-sale'))) {
                        string += 'On sale for <a class="price" data-value="' + dollars_paid.getAttribute('data-steam-sale-price') + '">';
                        if (region['symbol-position'] == 'before') {
                            string += region['symbol'] + dollars_paid.getAttribute('data-steam-sale-price');
                        } else {
                            string += dollars_paid.getAttribute('data-steam-sale-price') + region['symbol']; 
                        }
                        string += '</a>. ';
                    }
                    string += '<br>What did you pay?';

                } else {
                    if (parseInt(dollars_paid.getAttribute('data-on-sale'))) {
                        if (region['symbol-position'] == 'before') {
                            string = '<strong>Steam:</strong> ' + 
                                region['symbol'] + dollars_paid.getAttribute('data-steam-sale-price') +
                                ' / <del>' + region['symbol'] + dollars_paid.getAttribute('data-sale-price') + '</del>';
                        } else {
                            string = '<strong>Steam:</strong> ' + 
                                dollars_paid.getAttribute('data-steam-sale-price') + region['symbol'] + 
                                ' / <del>' + dollars_paid.getAttribute('data-sale-price') + region['symbol'] + '</del>';
                        }
                    } else {
                        if (region['symbol-position'] == 'before') {
                            string = '<strong>Steam:</strong> ' + region['symbol'] + dollars_paid.getAttribute('data-steam-price');
                        } else {
                            string = '<strong>Steam:</strong> ' + dollars_paid.getAttribute('data-steam-price') + region['symbol'];
                        }
                    }
                }
                return string;
            }

            on(dollars_paid, 'focus', function(evt) {
                // If the price has not been corrected, clear the default value.
                if (domClass.contains(evt.currentTarget, 'uncorrected')) {
                    dollars_paid.value = '';
                }

                // Give some life to the price links.
                query(dollars_paid).tooltip({
                    container: container,
                    trigger: 'manual',
                    placement: 'left',
                    html: true,
                    title: dollars_paid_tooltip_title
                }).tooltip('show');

                var signal = on(query('div.tooltip a.price'), 'click', function(evt) {
                    event.stop(evt);
                    signal.remove();

                    dollars_paid.value = evt.currentTarget.getAttribute('data-value');
                    on.emit(dollars_paid, 'change', {bubbles: true, cancelable: true});
                });
            });

            // Restore the current price when the price is empty.
            var restore_steam_price = function(evt) {
                query(dollars_paid).tooltip('destroy');

                if (!domClass.contains(evt.currentTarget, 'uncorrected') || dollars_paid.value != '') {
                    return;
                }
                dollars_paid.value = region['format-dollars'](dollars_paid.getAttribute('data-steam-price'));

                tabulate();
            };
            on(dollars_paid, 'blur', restore_steam_price);
            on(dollars_paid, 'keyup', function(evt) {
                if (evt.keyCode == keys.ENTER) {
                    dollars_paid.blur();
                }
            });

            // Update how many Dollars were paid for the game.
            on(dollars_paid, 'change', function(evt) {
                var hours_played_value = region['parse-hours'](hours_played.value);
                var cost_per_hour_value = 0.00;

                if (dollars_paid.value == '') {
                    var dollars_paid_value = region['parse-dollars'](dollars_paid.getAttribute('data-steam-price'));
                    if (isNaN(hours_played_value) || isNaN(dollars_paid_value)) {
                        return;
                    }
                    if (hours_played_value != 0) {
                        cost_per_hour_value = (dollars_paid_value / hours_played_value);
                        if (cost_per_hour_value > dollars_paid_value) {
                            cost_per_hour_value = dollars_paid_value;
                        }
                    }

                    analytics.trackEvent('Update Library', 'Cost');
                    rpc.request({
                        method: 'useraccountgame.update',
                        params: [
                            'id=' + id, {
                                dollars_paid: dollars_paid_value.toFixed(2),
                                cost_per_hour: cost_per_hour_value.toFixed(2),
                                corrected: false
                            }
                        ]
                    }, function(data) {
                        if (dollars_paid_value && hours_played_value) {
                            cost_per_hour.innerHTML = region['format-cost-per-hour'](data[0].cost_per_hour);
                        } else {
                            cost_per_hour.innerHTML = '&mdash;';
                        }

                        dollars_paid.blur();
                        dollars_paid.value = region['format-dollars'](dollars_paid_value);
                        domClass.add(dollars_paid, 'uncorrected');

                        tabulate();
                    });

                } else {
                    var dollars_paid_value = region['parse-dollars'](dollars_paid.value);
                    if (isNaN(hours_played_value) || isNaN(dollars_paid_value)) {
                        return;
                    }
                    if (hours_played_value != 0) {
                        cost_per_hour_value = (dollars_paid_value / hours_played_value);
                        if (cost_per_hour_value > dollars_paid_value) {
                            cost_per_hour_value = dollars_paid_value;
                        }
                    }

                    analytics.trackEvent('Update Library', 'Cost');
                    rpc.request({
                        method: 'useraccountgame.update',
                        params: [
                            'id=' + id, {
                                dollars_paid: dollars_paid_value.toFixed(2),
                                cost_per_hour: cost_per_hour_value.toFixed(2),
                                corrected: true
                            }
                        ]
                    }, function(data) {
                        if (dollars_paid_value && hours_played_value) {
                            cost_per_hour.innerHTML = region['format-cost-per-hour'](data[0].cost_per_hour);
                        } else {
                            cost_per_hour.innerHTML = '&mdash;';
                        }

                        dollars_paid.blur();
                        domClass.remove(dollars_paid, 'uncorrected');

                        tabulate();
                    });
                }
            });

            // Record ratings for games.
            // var rating = query('div.rating', game.cells[5]);
            var rating = game.cells[5].children[0];
            var links = query('a', rating);
            array.forEach(links, function(link) {
                on(link, 'click', function(evt) {
                    event.stop(evt);
                    var rating_value = parseInt(link.getAttribute('data-rating'));
                    if (isNaN(rating_value)) {
                        return;
                    }
                    if (rating_value < 1) {
                        rating_value = null;
                    }

                    analytics.trackEvent('Update Library', 'Rating');
                    rpc.request({
                        method: 'useraccountgame.update',
                        params: [
                            'id=' + id,
                            {rating: rating_value}
                        ]
                    }, function(data) {
                        rating.setAttribute('data-rating', data[0].rating);

                        query('div.rating div', game).removeClass('active');
                        for (var i = 1; i <= data[0].rating; ++i) {
                            query('div.rating-' + i, game).addClass('active');
                        }

                        if (data[0].rating > 0) {
                            query('a.rating-clear', game).removeClass('hidden');
                        } else {
                            query('a.rating-clear', game).addClass('hidden');
                        }

                        tabulate();
                    });
                });
            });

            // Ignore game links.
            // var ignore = query('a.ignore-game', game.cells[7])[0];
            var ignore = game.cells[7].children[0].children[0];
            on(ignore, 'click', function(evt) {
                event.stop(evt);
                var ignore = !domClass.contains(game, 'ignore');

                analytics.trackEvent('Update Library', 'Ignored');
                rpc.request({
                    method: 'useraccountgame.update',
                    params: [
                        'id=' + id, {ignore: ignore}
                    ]
                }, function(data) {
                    if (data[0].ignore) {
                        fx.animateProperty({
                            node: game,
                            duration: 150,
                            properties: {
                                opacity: {start: 1, end: 0} 
                            },
                            onEnd: function() {
                                game.innerHTML = '<td colspan="8"><div class="rollup-spacer"></div></td>';
                                fx.animateProperty({
                                    node: game.cells[0].children[0],
                                    properties: {height: {start: 40, end: 0}},
                                    duration: 150,
                                    onEnd: function() {
                                        domClass.add(game, 'ignore');
                                        _fix_table_stripes();
                                        tabulate();
                                    }
                                }).play();
                            }
                        }).play();
                    }
                });
            });

            // Finished checkboxes.
            // var finished = query('.finished-game', game.cells[6])[0];
            var finished = game.cells[6].children[0].children[0];
            on(finished, 'change', function(evt) {
                analytics.trackEvent('Update Library', 'Finished');
                rpc.request({
                    method: 'useraccountgame.update',
                    params: [
                        'id=' + id, {finished: finished.checked}
                    ]
                }, function(data) {
                    finished.checked = data[0].finished;
                    if (data[0].finished) {
                        finished.setAttribute('title', "I've finished playing this game.");
                    } else {
                        finished.setAttribute('title', 'Mark this game as finished.');
                    }

                    tabulate();
                });
            });
        });
    };

    //--------------------------------------------------------------------------
    var init = function() {
        fix_firefox_reload_autofill_bug();
        setup_help_tooltips();
        setup_last_game_update();
        setup_data_collection();
        setup_table_sorting(); 
        setup_filtering();
        setup_premium_nagging();
        setup_friends();
        setup_game_thumbnails();

        pre_filter_library();
        tabulate();
    };
    return init;
});
