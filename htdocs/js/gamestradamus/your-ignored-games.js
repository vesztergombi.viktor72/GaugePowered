define([
    "dojo/query",
    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/_base/fx",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-class",
    "atemi/io/jsonrpc",
    "atemi/util/TableSort",

    "dojo/NodeList-dom",
    "dojo/domReady!"
], function(query, array, event, fx, on, dom, domClass, jsonrpc, TableSort) {

    var init = function() {
        var rpc = jsonrpc('/jsonrpc');
        var table = dom.byId('game_list');
        var games = query('tbody tr.game', table);

        array.forEach(games, function(game) {
            var id = parseInt(game.getAttribute('data-user-account-game-id'));

            // Restore game links.
            var ignore = query('a.ignore-game', game)[0];
            on(ignore, 'click', function(evt) {
                event.stop(evt);
                var ignore = domClass.contains(game, 'ignore');

                rpc.request({
                    method: 'useraccountgame.update',
                    params: [
                        'id=' + id, {ignore: ignore}
                    ]
                }, function(data) {
                    if (!data[0].ignore) {
                        fx.animateProperty({
                            node: game,
                            duration: 250,
                            properties: {
                                opacity: {start: 1, end: 0} 
                            },
                            onEnd: function() {
                                domClass.add(game, 'ignore');
                            }
                        }).play();
                    } else {
                        domClass.remove(game, 'ignore');
                    }
                });
            });
        });

        new TableSort(
            table, [0, 0], [
                TableSort.parseInt,
                TableSort.textContent,
                TableSort.parseFloat
            ],
            function(data) {
                // Fix the position counter for the rows.
                array.forEach(data, function(row, i) {
                    row[0].cells[0].innerHTML = '<div>' + (i + 1) + '</div>';
                });
            }
        ); 
    };
    return init;
})
