define([
    "dojo/request",
    "dojo/query",
    "bootstrap/Typeahead"
], function(request, query, typeahead) {
    var init = function() {
        var timeout = 250;
        var results = null;
        var results_query = null;
        var search_deferred = null;
        var latest_query = null;

        // javasript function to search within an array of results.
        function search_within_results(query, results) {
            query = query.toLowerCase();
            var matches = [];
            for(var i = 0; i < results.length; ++i) {
                var possibility = results[i];
                if (possibility.toLowerCase().indexOf(query) != -1) {
                    matches.push(possibility);
                }
            }
            return matches;
        }

        query("#search-form input").typeahead({
            minLength: 3,
            source: function(query, process) {
                var is_subsearch = false;
                if (results_query) {
                    is_subsearch = query.indexOf(results_query) == 0;
                }
                latest_query = query;

                // If this is a subsearch of a search we're executing
                // then don't execute another one, just wait for this one to be
                // done and store the new more specific search value.
                if (search_deferred && is_subsearch) {
                    return search_deferred;
                }

                if (results && is_subsearch) {
                    process(
                        search_within_results(
                            latest_query, results.names
                        )
                    );
                    return;
                }

                // If this is a new search (or at least not a subsearch)
                // but we have an outstanding search request, cancel it and
                // try a new one.
                if (search_deferred) {
                    search_deferred.cancel();
                    results = null;
                }

                results_query = query;
                search_deferred = request(
                    '/search-json?query='+query,
                    {handleAs:'json'}
                ).then(function (data) {
                    // now we have results so clear out the
                    search_deferred = null;
                    results = data;

                    // If the search we sent to the server
                    // is our latest data, use it outright.
                    if (latest_query == query) {
                        process(results.names)
                    } else {
                        // otherwise search within the results.
                        process(
                            search_within_results(
                                latest_query, results.names
                            )
                        );
                    }
                });
                return search_deferred;
            }
        });
        query("#search-form input").on("change", function(event) {
            game = event.target.value;
            if (results && results.urls[game]) {
                window.location = results.urls[game];
            }
        });
    };
    return init;
});
