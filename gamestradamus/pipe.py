# -*- coding: utf-8 -*-
from __future__ import absolute_import
"""
Setup the basic gamestradamus pipes.
"""

import math
import random
import decimal
import datetime
import cherrypy
from geniusql import logic
import csv


from celery.result import AsyncResult

import cream
import cream.tools
import cream.tools.rpc
import cream.auth
import cream.defaults
import cream.rpc
import cream.rpc.json
from cream.sessions import redis
import os
import json

import hydro
import plumbing.auth
import plumbing.email

from gamestradamus import filters, units, timeutils, plugins
from gamestradamus.form import to_decimal
from gamestradamus.taskqueue import tasks, celery
import gamestradamus.handlers

#-------------------------------------------------------------------------------
def __register__():
    return units.__register__()

#-------------------------------------------------------------------------------
def __plugins__(engine):
    if cherrypy.config.get('environment') == 'development':
        engine.DEV_sendemail = plugins.DEV_SendEmailMonitor(engine, frequency=10)
        engine.DEV_sendemail.subscribe()

    cream.defaults.jinja2(
        "gamestradamus",
        filters=filters.filters
    )

#-------------------------------------------------------------------------------
def __fixture__(box):
    pass

#-------------------------------------------------------------------------------
def _should_welcome(user_attrs):
    """Determines whether we should re-welcome them or not.
    """

    # if we have welcomed them in the past, then we shouldn't welcome them now
    if user_attrs.welcomed_since:
        return False

    # If we haven't started it yet, then we may as well
    if not user_attrs.welcome_started:
        return True

    # If we started welcoming them, but didn't finish within 5 minutes
    # welcome them again.
    ago = timeutils.utcnow() - datetime.timedelta(minutes=5)
    if user_attrs.welcome_started < ago:
        return True

    # Default is to not run it again
    return False

#-------------------------------------------------------------------------------
def post_success(user_account, return_url):
    """Ensure associated UserAccount Entities have been created."""

    # Initialize the user_account attributes.
    user_attrs = user_account.UserAccountAttributes()
    if user_attrs is None:
        user_attrs = units.UserAccountAttributes(**{
            'user_account_id': user_account.id
        })
        user_account.sandbox.memorize(user_attrs)

    routing_key = "tasks.plebs.update-games"
    if user_attrs and user_attrs.premium_account:
        routing_key = "tasks.premium.update-games"

    # Initialize the user account statistics
    stats = user_account.UserAccountStatistics()
    if stats is None:
        stats = units.UserAccountStatistics(**{
            'user_account_id': user_account.id
        })
        user_account.sandbox.memorize(stats)

    # Update either game statistics or predictions.
    steam_id_64 = user_account.openid.rsplit('/', 1)[-1]
    if user_attrs.welcomed_since:
        result = tasks.steam_update_hours_in_game.apply_async(**{
            'args': [steam_id_64],
            'routing_key': routing_key,
        })
    else:
        # Now we must determine if we should run the welcome task or not .
        if _should_welcome(user_attrs):
            user_attrs.predictions_updating = True

            # Note that we don't have to explicitly route this one as
            # we have set this task to always route to tasks.welcome
            result = tasks.welcome.apply_async(**{
                'args': (steam_id_64, 200, 120)
            })
            user_attrs.welcome_started = timeutils.utcnow()
            user_attrs.private_profile_since = None

        # Send the user to the welcome page.
        had_to_lock = False
        if not cherrypy.session.locked:
            had_to_lock = True
            cherrypy.session.acquire_lock()
        try:
            cherrypy.session[plumbing.auth.tools.SESSION_IDENTITY_KEY] = user_account.id
        finally:
            if had_to_lock:
                cherrypy.session.release_lock()

        raise cherrypy.HTTPRedirect(return_url or '/profile/welcome/')

#-------------------------------------------------------------------------------
class UserAccountGameCUDLHandler(plumbing.auth.handlers.UserAccountEntityTypeCUDLHandler):
    """Restrict access to items owned by the requesting UserAccount."""

    """
    #---------------------------------------------------------------------------
    @property
    def list_rql(self):
        list_rql = super(UserAccountGameCUDLHandler, self).list_rql
        list_rql.variables.update({
            'id': cream.rql.Number(**{
                'ops': ['eq'],
                'conjunctions': ['and'],
                'count': 1,
                'null': False,
                'required': True
            })
        })
        return list_rql
    """
    pass

#-------------------------------------------------------------------------------
def __entity_types__():
    """Let Hydro know about our Entity Type definitions."""
    return {}


#-------------------------------------------------------------------------------
def __handlers__(root):
    """
    Setup the necessary handlers
    """
    local_dir = os.path.join(os.getcwd(), os.path.dirname(__file__))
    root.js = cherrypy.tools.staticdir.handler(
        section='/js',
        dir=os.path.join(local_dir, '..', 'htdocs', 'js'),
        match=r'\.(css|gif|html?|ico|jpe?g|js|png|swf|xml|woff|eot|svg|ttf|txt)$'
    )
    root.bootstrap = cherrypy.tools.staticdir.handler(
        section='/bootstrap',
        dir=os.path.join(local_dir, '..', 'htdocs', 'bootstrap'),
        match=r'\.(css|gif|html?|ico|jpe?g|js|png|swf|xml|woff|eot|svg|ttf|txt)$'
    )
    root.css = cherrypy.tools.staticdir.handler(
        section='/css',
        dir=os.path.join(local_dir, '..', 'htdocs', 'css'),
        match=r'\.(css|gif|html?|ico|jpe?g|js|png|swf|xml|woff|eot|svg|ttf|txt)$'
    )
    root.images = cherrypy.tools.staticdir.handler(
        section='/images',
        dir=os.path.join(local_dir, '..', 'htdocs', 'images'),
        match=r'\.(css|gif|html?|ico|jpe?g|js|png|swf|xml|woff|eot|svg|ttf|txt)$'
    )
    root.favicon_ico = cherrypy.expose(cherrypy.tools.staticfile.handler(
        filename=os.path.join(local_dir, '..', 'htdocs', 'favicon.ico')
    ), alias='favicon.ico')
    root.robots_txt = cherrypy.expose(cherrypy.tools.staticfile.handler(
        filename=os.path.join(local_dir, '..', 'htdocs', 'robots.txt')
    ), alias='robots.txt')
    root.apple_touch_icon_png = cherrypy.expose(cherrypy.tools.staticfile.handler(
        filename=os.path.join(local_dir, '..', 'htdocs', 'apple-touch-icon.png')
    ), alias='apple-touch-icon.png')

    #---------------------------------------------------------------------------
    root.auth = plumbing.auth.handlers.Handler(**{
        'require_invitation': False,
        'success_redirect_url': '/profile/',
        'post_success': post_success,
    })
    root.auth.openid = cream.auth.OpenIDHandler(**{
        'mounted_on': '/auth/openid/',
        'success': root.auth.success,
        'failure': root.auth.failure,
        'fields': ['fullname', 'nickname', 'email']
    })
    root.auth.openid._providers = {
        'steam': 'http://steamcommunity.com/openid/'
    }

    #---------------------------------------------------------------------------
    def error_page(status, message, traceback, version):
        """Show an appropriate error page to the user."""

        return cream.tools.JinjaTemplate.render(
            '/gamestradamus/error.html', {
                'status': status,
                'message': message,
                'traceback': traceback,
                'version': version
            }
        )

    cherrypy.config.update({
        'error_page.default': error_page
    })

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/error.html')
    def signin(self, *args, **kwargs):
        return {
            'status': 'Sign in Failed.',
            'message': """
                Steam is currently experiencing issues. You may want to try again later.<br>(%s)""" % (
                cherrypy.session.get(plumbing.auth.tools.SESSION_AUTH_ERROR, None),
            )
        }
    root.auth.signin = signin.__get__(root.auth)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/home.html')
    def index(self, *args, **kwargs):

        request = cherrypy.serving.request
        if getattr(request, 'user_account', None):
            raise cherrypy.HTTPRedirect('/profile/games/')

        return {}
    root.index = index.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias='sitemap')
    @cream.tools.JinjaTemplate('/gamestradamus/sitemap.xml')
    def sitemap(self):
        request = cherrypy.serving.request
        price_attr = request.user_region['game_attributes']['price']
        available_since_attr = request.user_region['game_attributes']['available_since']

        request = cherrypy.serving.request
        games = request.sandbox.store.view((
            units.Game,
            lambda x: [x.id],
            lambda x: (
                x.is_dlc == False and
                getattr(x, available_since_attr) != None
            ),
        ))
        games = [x[0] for x in games]

        cherrypy.response.headers['Content-Type'] = "application/xml"
        return {
            'games': games
        }
    root.sitemap = sitemap.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/faq.html')
    def faq(self):
        return {}
    root.faq = faq.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias="privacy-policy")
    @cream.tools.JinjaTemplate('/gamestradamus/privacy-policy.html')
    def privacy_policy(self):
        return {}
    root.privacy_policy = privacy_policy.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias='terms-of-service')
    @cream.tools.JinjaTemplate('/gamestradamus/terms-of-service.html')
    def terms_of_service(self):
        return {}
    root.terms_of_service = terms_of_service.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/unsubscribe.html')
    def unsubscribe(self, permanent_unsubscribe_code, for_realz=False, *args, **kwargs):
        request = cherrypy.serving.request
        box = request.sandbox

        # Get the email they are unsubscribing.
        user_account_email = box.UserAccountEmail(
                permanent_unsubscribe_code=permanent_unsubscribe_code)

        # if there isn't one this page is a 404
        if not user_account_email:
            raise cherrypy.NotFound()

        unsubscribed_already = box.UnsubscribedEmail(email=user_account_email.email)
        if for_realz and request.method == "POST" and not unsubscribed_already:
            # Only bother with the unsubscribe request if they are not already
            # unsubscribed AND they clicked the checkbox.
            request.internal_api.call(
                'email.unsubscribe.create',
                '',
                {
                    'email': user_account_email.email,
                }
            )
            raise cherrypy.HTTPRedirect(cherrypy.url())

        return {
            'user_account_email': user_account_email,
            'unsubscribed_already': unsubscribed_already,
        }
    root.unsubscribe = unsubscribe.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias="ce")
    def confirm_email(self, confirmation_code=None, **kwargs):
        request = cherrypy.serving.request

        if not confirmation_code:
            raise cherrypy.NotFound()

        user_account_email = request.sandbox.UserAccountEmail(**{
            'confirmation_code': confirmation_code
        })
        if not user_account_email:
            raise cherrypy.NotFound()

        user_account_email.date_confirmed = timeutils.utcnow()
        user_account_email.confirmation_code = None
        raise cherrypy.HTTPRedirect("/profile/settings/")
    root.ce = confirm_email.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias="ai")
    @cream.tools.JinjaTemplate('/gamestradamus/accept-invitation.html')
    def accept_invitation(self, invite_code=None, **kwargs):
        request = cherrypy.serving.request

        if not invite_code:
            raise cherrypy.NotFound()

        invitation = request.sandbox.UserAccountInvitation(**{
            'invite_code': invite_code,
        })
        if not invitation:
            raise cherrypy.NotFound()

        ago = timeutils.utcnow() - datetime.timedelta(days=5)
        if invitation.created < ago:
            # Invitations have a 5 day expiry period.
            raise cherrypy.NotFound()

        if getattr(request, 'user_account', None) is not None:
            # You can't invite yourself to be your own friend, sorry "forever alone's".
            if invitation.user_account_id == request.user_account.id:
                raise cherrypy.NotFound()

        if request.method == 'POST':
            # For a POST request to succeed, you must be SIGNED in.
            if getattr(request, 'user_account', None) is None:
                raise cherrypy.NotFound()

            # Update the invitation and create the "Friend" records.
            invitation.friend_id = request.user_account.id
            invitation.invite_code = None

            friendship = request.sandbox.UserAccountFriend(**{
                'user_account_id': invitation.user_account_id,
                'friend_id': invitation.friend_id
            })
            if friendship is None:
                friendship = units.UserAccountFriend(**{
                    'user_account_id': invitation.user_account_id,
                    'friend_id': invitation.friend_id,
                    'since': timeutils.utcnow(),
                })
                request.sandbox.memorize(friendship)

            friendship = request.sandbox.UserAccountFriend(**{
                'user_account_id': invitation.friend_id,
                'friend_id': invitation.user_account_id
            })
            if friendship is None:
                friendship = units.UserAccountFriend(**{
                    'user_account_id': invitation.friend_id,
                    'friend_id': invitation.user_account_id,
                    'since': timeutils.utcnow(),
                })
                request.sandbox.memorize(friendship)

            raise cherrypy.HTTPRedirect('/member/%s/games/' % invitation.user_account_id)

        else:
            return {
                'invitation': invitation,
                'requestor': invitation.UserAccount(),
            }

    root.ai = accept_invitation.__get__(root)

    #---------------------------------------------------------------------------
    def _search(self, query=""):
        request = cherrypy.serving.request
        price_attr = request.user_region['game_attributes']['price']
        available_since_attr = request.user_region['game_attributes']['available_since']

        # security issue: query MUST be escaped!
        query = query.strip(' \t\n')
        if len(query) < 3:
            return []

        query = '%%%s%%' % query.lower().replace("'", "")
        games, _ = request.sandbox.store.db.fetch("""
            SELECT id, name, owner_count
            FROM "Game"
            WHERE name ILIKE '%s'
                AND is_dlc = false
                AND canonical_game_id IS NULL
                AND %s IS NOT NULL
        """ % (query, available_since_attr))
        games.sort(key=lambda g: g[2], reverse=True)
        return games
    root._search = _search.__get__(root)

    #-----------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/search.html')
    def search(self, query=""):
        request = cherrypy.serving.request

        games = self._search(query)
        results = [request.sandbox.Game(id=g[0]) for g in games]

        return {
            "search": {
                "query": query,
                "results": results,
            },
        }
    root.search = search.__get__(root)

    #-----------------------------------------------------------------------
    @cherrypy.expose(alias="search-json")
    def search_json(self, query=""):
        request = cherrypy.serving.request

        games = self._search(query)
        results = {
            'names': [g[1] for g in games],
        }
        user_account = getattr(request, 'user_account', None)
        if user_account:
            results.update({
                'urls': dict([(
                    g[1],
                    '/profile/game/%s/' % g[0]
                ) for g in games])
            })
        else:
            results.update({
                'urls': dict([(
                    g[1],
                    '/game/%s/' % g[0]
                ) for g in games])
            })

        cherrypy.response.headers['Content-Type'] = "application/json"
        return json.dumps(results)
    root.search_json = search_json.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    def app(self, app_id, **kwargs):
        request = cherrypy.serving.request

        game = request.sandbox.Game(**{'app_id': app_id})
        if game is not None:
            raise cherrypy.HTTPRedirect('/game/%s/' % game.id)

        raise cherrypy.NotFound()
    root.app = app.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/game.html')
    def game(self, *args, **kwargs):
        request = cherrypy.serving.request

        if len(args) < 1:
            raise cherrypy.NotFound()
        game_id = args[0]

        # Send people that are signed in to their game details page.
        user_account = getattr(request, 'user_account', None)
        if user_account is not None:
            raise cherrypy.HTTPRedirect('/profile/game/%s/' % game_id)

        game = request.sandbox.Game(**{'id': game_id})
        if game is None:
            raise cherrypy.NotFound()
        if game.canonical_game_id:
            raise cherrypy.HTTPRedirect(
                '/game/%s/' % game.canonical_game_id
            )

        return {
            'game': game,
            'now': datetime.datetime.now(),
        }
    root.game = game.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/browse.html')
    def browse(self, *args, **kwargs):
        request = cherrypy.serving.request

        price_attr = request.user_region['game_attributes']['price']
        sale_price_attr = request.user_region['game_attributes']['sale_price']
        on_sale_since_attr = request.user_region['game_attributes']['on_sale_since']
        available_since_attr = request.user_region['game_attributes']['available_since']

        order = {
            'cheapest': 'cheapest',
            'gauge': 'gauge',
            'hours': 'hours',
        }.get(kwargs.get('order', 'gauge'), 'gauge')
        genre = {
            'action': 'Action',
            'adventure': 'Adventure',
            'strategy': 'Strategy',
            'rpg': 'RPG',
            'indie': 'Indie',
            'massively multiplayer': 'Massively Multiplayer',
            'platformer': 'Platformer',
            'casual': 'Casual',
            'simulation': 'Simulation',
            'racing': 'Racing',
            'sports': 'Sports',
            'early access': 'Early Access',
        }.get(kwargs.get('genre', None), None)
        if genre:
            genre = request.sandbox.Genre(name=genre)
        price = to_decimal(kwargs.get('price', None), request.user_region)

        expr = logic.Expression(
            lambda x: x.is_dlc == False and getattr(x, available_since_attr) != None
        )
        join = units.Game
        if genre:
            expr = expr & logic.Expression(lambda x, y: y.genre_id == genre.id)
            join = join << units.GameGenre
        if price:
            expr = expr & logic.Expression(lambda x: getattr(x, sale_price_attr) <= price)

        games = request.sandbox.store.view((
            join,
            lambda x: (x.id, getattr(x, price_attr), getattr(x, sale_price_attr), getattr(x, on_sale_since_attr), x.median_hours_played, x.owner_count),
            expr
        ))
        x_id, x_price, x_sale_price, x_on_sale_since, x_median_hours, x_owner_count = range(6)

        # Reorder the results.
        if order == 'gauge':
            games = [g for g in games if g[x_price]]

            def key(x):
                min_price = min(x[x_price] or 0, x[x_sale_price] or 0)
                if x[x_median_hours] and ((x[x_owner_count] or 0) > 19):
                    return (min(
                        min_price / x[x_median_hours],
                        min_price
                    ), x[x_id])
                else:
                    return (min_price, x[x_id])

            games.sort(**{'key': key})

        elif order == 'hours':
            games.sort(**{
                'key': lambda x: (x[x_median_hours] or 0, x[x_id]),
                'reverse': True
            })
        else:
            games = [g for g in games if g[x_price]]
            games.sort(**{
                'key': lambda x: (x[x_sale_price], x[x_id])
            })

        # Show only the games that are on the current page.
        total_count = len(games)
        page_count = int(math.ceil(len(games) / 24.0));
        try:
            page = int(kwargs.get('page', 1))
            if page < 1:
                page = 1
            elif page > page_count:
                page = page_count
        except (ValueError, TypeError), e:
            page = 1
        games = games[((page - 1) * 24):(page * 24)]

        # Figure out which pages to show to the user.
        lower = max(1, min(page_count - 4, page - 2))
        pages = [(lower + x) for x in range(5) if (lower + x) <= page_count]

        # Fetch units for the included game ids.
        game_id_list = [g[x_id] for g in games]
        games = request.sandbox.recall(
             units.Game,
             lambda x: x.id in game_id_list
        )

        # Sort the page results to ensure that they are consistent with the global sort.
        if order == 'gauge':
            games.sort(**{
                'key': lambda x: x.community_value,
            })
        elif order == 'hours':
            games.sort(**{
                'key': lambda x: (x.median_hours_played or 0, x.id),
                'reverse': True
            })
        else:
            games.sort(**{
                'key': lambda x: x.sale_price,
            })

        # Split the list into two lists for display
        games_one = []
        games_two = []

        for x in range(0, len(games)):
            if x % 2:
                games_two.append(games[x])
            else:
                games_one.append(games[x])

        return {
            'games_one': games_one,
            'games_two': games_two,
            'order': order,
            'page': page,
            'pages': pages,
            'page_count': page_count,
            'total_count': total_count,
            'genre': genre.name.lower() if genre else None,
            'price': price,
        }
    root.browse = browse.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/sales.html')
    def sales(self, **kwargs):
        request = cherrypy.serving.request

        price_attr = request.user_region['game_attributes']['price']
        sale_price_attr = request.user_region['game_attributes']['sale_price']
        on_sale_since_attr = request.user_region['game_attributes']['on_sale_since']
        available_since_attr = request.user_region['game_attributes']['available_since']

        order = {
            'recent': 'recent',
            'savings': 'savings',
            'cheapest': 'cheapest',
            'gauge': 'gauge',
            'hours': 'hours',
        }.get(kwargs.get('order', 'recent'), 'recent')
        genre = {
            'action': 'Action',
            'adventure': 'Adventure',
            'strategy': 'Strategy',
            'rpg': 'RPG',
            'indie': 'Indie',
            'massively multiplayer': 'Massively Multiplayer',
            'platformer': 'Platformer',
            'casual': 'Casual',
            'simulation': 'Simulation',
            'racing': 'Racing',
            'sports': 'Sports',
            'early access': 'Early Access',
        }.get(kwargs.get('genre', None), None)
        if genre:
            genre = request.sandbox.Genre(name=genre)
        price = to_decimal(kwargs.get('price', None), request.user_region)

        expr = logic.Expression(
            lambda x: getattr(x, on_sale_since_attr) is not None and x.is_dlc == False and getattr(x, available_since_attr) != None
        )
        join = units.Game
        if genre:
            expr = expr & logic.Expression(lambda x, y: y.genre_id == genre.id)
            join = join << units.GameGenre
        if price:
            expr = expr & logic.Expression(lambda x: getattr(x, sale_price_attr) <= price)

        # Which games are on sale?
        games = request.sandbox.store.view((
            join,
            lambda x, y: (x.id, getattr(x, price_attr), getattr(x, sale_price_attr), getattr(x, on_sale_since_attr), x.median_hours_played, x.owner_count),
            expr
        ))
        x_id, x_price, x_sale_price, x_on_sale_since, x_median_hours, x_owner_count = range(6)

        # Compile a list of unique genres in the result set.
        expr = logic.Expression(
            lambda x, y, z: getattr(x, on_sale_since_attr) is not None and x.is_dlc == False and getattr(x, available_since_attr) != None
        )
        if price:
            expr = expr & logic.Expression(
                lambda x, y, z: getattr(x, sale_price_attr) <= price
            )
        genres = request.sandbox.store.view((
                units.Game << units.GameGenre << units.Genre,
                lambda x, y, z: [x.id, z.name],
                expr
            ))
        genres = set([x[1].lower() for x in genres if x[1]])

        # Reorder the results.
        if order == 'recent':
            games.sort(**{
                'key': lambda x: x[x_on_sale_since],
                'reverse': True,
            })
        elif order == 'savings':
            games.sort(**{
                'key': lambda x: ((x[x_price] - x[x_sale_price]) / x[x_price]),
                'reverse': True
            })
        elif order == 'gauge':

            def key(x):
                min_price = min(x[x_price] or 0, x[x_sale_price] or 0)
                if x[x_median_hours] and ((x[x_owner_count] or 0) > 19):
                    return min(
                        min_price / x[x_median_hours],
                        min_price
                    )
                else:
                    return min_price

            games.sort(**{'key': key})

        elif order == 'hours':
            games.sort(**{
                'key': lambda x: (x[x_median_hours] or 0, x[x_id]),
                'reverse': True
            })
        else:
            games.sort(**{
                'key': lambda x: x[x_sale_price],
            })

        # Show only the games that are on the current page.
        total_count = len(games)
        page_count = int(math.ceil(len(games) / 24.0));
        try:
            page = int(kwargs.get('page', 1))
            if page < 1:
                page = 1
            elif page > page_count:
                page = page_count
        except (ValueError, TypeError), e:
            page = 1
        games = games[((page - 1) * 24):(page * 24)]

        # Figure out which pages to show to the user.
        lower = max(1, min(page_count - 4, page - 2))
        pages = [(lower + x) for x in range(5) if (lower + x) <= page_count]

        # Fetch units for the included game ids.
        game_id_list = [g[x_id] for g in games]
        games = request.sandbox.recall(
             units.Game,
             lambda x: x.id in game_id_list
        )

        # Sort the page results to ensure that they are consistent with the global sort.
        if order == 'recent':
            games.sort(**{
                'key': lambda x: x.on_sale_since,
                'reverse': True,
            })
        elif order == 'savings':
            games.sort(**{
                'key': lambda x: (x.price - x.sale_price) / x.price,
                'reverse': True
            })
        elif order == 'gauge':
            games.sort(**{
                'key': lambda x: x.community_value,
            })
        elif order == 'hours':
            games.sort(**{
                'key': lambda x: (x.median_hours_played or 0, x.id),
                'reverse': True
            })
        else:
            games.sort(**{
                'key': lambda x: x.sale_price,
            })

        games_one = []
        games_two = []

        for x in range(0, len(games)):
            if x % 2:
                games_two.append(games[x])
            else:
                games_one.append(games[x])

        return {
            'games_one': games_one,
            'games_two': games_two,
            'order': order,
            'page': page,
            'pages': pages,
            'page_count': page_count,
            'total_count': total_count,
            'genre': genre.name.lower() if genre else None,
            'genres': genres,
            'price': price,
        }
    root.sales = sales.__get__(root)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/giveaway.html')
    def giveaway(self, **kwargs):
        request = cherrypy.serving.request

        # Send people that are signed in to their giveaway page.
        user_account = getattr(request, 'user_account', None)
        if user_account is not None:
            raise cherrypy.HTTPRedirect('/profile/giveaway/')

        # Figure out which game we're giving away this week
        now = timeutils.utcnow()
        local_dir = os.path.join(os.getcwd(), os.path.dirname(__file__))
        with open(os.path.join(local_dir, '..', 'weekly-game-giveaway-schedule.csv')) as csvfile:
            reader = csv.reader(csvfile)
            reader.next()
            for row in reader:
                d = datetime.datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
                d = d.replace(tzinfo=cream.timeutils.Timezone.from_cache(0))
                if d < now:
                    continue
                break

            id = int(row[1])

        return {
            'game': request.sandbox.Game(id=id),
            'now': datetime.datetime.now(),
        }
    root.giveaway = giveaway.__get__(root)

    #---------------------------------------------------------------------------
    root.profile = gamestradamus.handlers.ProfileHandler()
    root.member = gamestradamus.handlers.MemberHandler()
    root.payment = gamestradamus.handlers.PaymentHandler()
    root.gauge_admin = gamestradamus.handlers.admin.AdminHandler()

    #---------------------------------------------------------------------------
    # Setup the API handlers
    root.jsonrpc = cream.rpc.json.Dispatcher()
    root.jsonrpc.useraccountgame = \
        gamestradamus.handlers.AuthenticatedUserAccountGameCUDLHandler()
    root.jsonrpc.useraccountgamewatch = \
        gamestradamus.handlers.AuthenticatedUserAccountGameWatchCUDLHandler()
    root.jsonrpc.useraccountemail = \
        gamestradamus.handlers.AuthenticatedUserAccountEmailCUDLHandler()
    root.jsonrpc.useraccountattributes = \
        gamestradamus.handlers.AuthenticatedUserAccountAttributesCUDLHandler()
    root.jsonrpc.useraccounttag = \
        gamestradamus.handlers.AuthenticatedUserAccountTagCUDLHandler()
    root.jsonrpc.useraccountinvitation = \
        gamestradamus.handlers.AuthenticatedUserAccountInvitationCUDLHandler()
    root.jsonrpc.useraccountfriend = \
        gamestradamus.handlers.AuthenticatedUserAccountFriendCUDLHandler()

    root.jsonrpc.predictions = gamestradamus.handlers.PredictionHandler()

    internal_api = cream.rpc.json.Dispatcher()
    internal_api.email = plumbing.email.handlers.EmailHandler()
    internal_api.email.unsubscribe = plumbing.email.handlers.UnsubscribedEmailHandler()
    cherrypy.config.update({
        'tools.multi_api.on': True,
        'tools.multi_api.dispatchers': {
            'api': {'dispatcher': root.jsonrpc},
            'internal_api': {'dispatcher': internal_api}
        }
    })

