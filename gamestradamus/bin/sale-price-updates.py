
from gamestradamus.taskqueue import tasks
from gamestradamus import cli, timeutils, units


#------------------------------------------------------------------------------
def remove_qs(url):
    from urlparse import urlparse
    o = urlparse(url)
    return o.scheme + "://" + o.netloc + o.path

#------------------------------------------------------------------------------
def get_id(url):
    url = remove_qs(url)
    parts = url.split("/")
    return parts[4]


#------------------------------------------------------------------------------
def filter_urls_to_individual_games(anchors):
    sale_href = 'http://store.steampowered.com/app/'
    franchise_href = 'http://store.steampowered.com/sale/'
    package_href = 'http://store.steampowered.com/sub/'
    ids = set([get_id(a['href']) for a in anchors if a['href'].startswith(sale_href)])
    packages = [a['href'] for a in anchors if a['href'].startswith(franchise_href)]
    subs = set([get_id(a['href']) for a in anchors if a['href'].startswith(package_href)])
    for url in packages:
        package_response = requests.get(url)
        soup = BeautifulSoup(package_response.text)
        games_in_package = soup.select('a')
        for anchor in games_in_package:
            url = anchor['href']
            if url.startswith(sale_href):
                ids.add(get_id(url))
            elif url.startswith(package_href):
                subs.add(get_id(url))

    print "SUBS!!!!", subs
    # Remove duplicates
    return list(set(ids)), list(set(subs))

#------------------------------------------------------------------------------
def get_front_page_sales():
    front_page_response = requests.get("http://store.steampowered.com/?cc=us")
    soup = BeautifulSoup(front_page_response.text)
    featured_blocks = soup.select('.home_ctn .home_page_content')
    daily_block, flash_block, community_block = featured_blocks[1:4]
    sale_href = 'http://store.steampowered.com/app/'
    daily_deal_ids, daily_deal_package_ids = filter_urls_to_individual_games(daily_block.select("a"))
    flash_ids, flash_package_ids = filter_urls_to_individual_games(flash_block.select("a"))
    monster_ids, monster_package_ids = filter_urls_to_individual_games(community_block.select("a"))

    store = cli.storage_manager()
    sandbox = store.new_sandbox()

    for app_id in daily_deal_ids + flash_ids + monster_ids + daily_deal_package_ids + flash_package_ids + monster_package_ids:
        game = sandbox.unit(units.Game, app_id=app_id)
        tasks.update_game_price.apply_async(**{
            'args': (game.id, 'us'),
            'routing_key': 'tasks.premium.parse-game-price',
        })


get_front_page_sales()
