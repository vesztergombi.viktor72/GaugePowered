"""Sample timer copied off the internets.
"""
import time

class Timer(object):
    def __init__(self, verbose=False):
        self.verbose = verbose
        self.secs = 0
        self.msecs = 0

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs += (self.end - self.start)
        self.msecs = (self.secs * 1000) # millisecs
        if self.verbose:
            print 'elapsed time: %f ms' % self.msecs

