#. coding=utf-8
"""
Helpers for our command line scripts.
"""
import os
import ConfigParser
from dejavu import storage

import hydro.atoms
import plumbing.auth
import plumbing.email
import gamestradamus.units

local_dir = os.path.join(os.getcwd(), os.path.dirname(__file__))

#-------------------------------------------------------------------------------
default_configs = [
        "deploy.conf",
        "dev.conf",
        os.path.join(local_dir, 'dev.conf'),
    ]

#-------------------------------------------------------------------------------
def find_valid_configuration(config=None):
    if config is not None:
        if not os.path.exists(config):
            raise RuntimeError(
                "Specified configuration file [%s] does not exist." % config
            )
        else:
            return config

    if config is None:
        for config_file in default_configs:
            if os.path.exists(config_file):
                return config_file
                continue

    raise RuntimeError(
        "No configuration file specified and none of the defaults found"
    )

#-------------------------------------------------------------------------------
def get_configuration(config=None):
    config_file = find_valid_configuration(config)

    from cherrypy.lib import reprconf
    return reprconf.as_dict(config_file)

#-------------------------------------------------------------------------------
def storage_manager(config=None, conflicts='error'):
    """Creates the appropriate storage manager for gamestradamus.
    """
    config = get_configuration(config)
    db_conf = config.get('global', {}).get('hydro.database.config', None)

    store = storage.resolve('psycopg2', db_conf)
    store.db.encoding = "UTF8"

    units = {}
    units.update(hydro.atoms.__register__())
    units.update(plumbing.auth.pipe.__register__())
    units.update(gamestradamus.units.__register__())
    units.update(plumbing.email.units.__register__())

    store.register_all(units)
    store.map_all(conflicts)
    return store

#-------------------------------------------------------------------------------
def parse_pg_params(connection_string):
    parts = connection_string.split(" ")
    params = {}
    for part in parts:
        key, value = part.split("=")
        params[key] = value
    return params

#-------------------------------------------------------------------------------
def get_database_configuration(config=None):
    global_conf = get_global_config(config)
    db_conf = global_conf.get('hydro.database.config', {})
    return parse_pg_params(db_conf['connections.Connect'])

#-------------------------------------------------------------------------------
def get_global_config(config=None):
    config_file = find_valid_configuration(config)

    from cherrypy.lib import reprconf
    config = reprconf.as_dict(config_file)
    return config.get('global', {})
