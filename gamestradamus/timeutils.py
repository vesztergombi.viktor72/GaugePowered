# coding: utf-8
"""
A simple set of helpers for gamestradamus to aid in getting certain types of time
"""
import datetime
import cream

#-----------------------------------------------------------------------------
def utcnow():
    """
    Returns the current time in UTC with timezone attached.
    """
    return datetime.datetime.now(cream.timeutils.Timezone.from_cache(0))

#-----------------------------------------------------------------------------
def to_string(_datetime):
    """
    Convert the given datetime into a string format suitable for JSON RPC
    """
    return cream.timeutils.Converters.iso_from_datetime(_datetime)

#-----------------------------------------------------------------------------
def from_string(_datetime_string):
    """
    Convert the datetime from a string into a datetime.
    """
    return cream.timeutils.Converters.datetime_from_iso(_datetime_string)

