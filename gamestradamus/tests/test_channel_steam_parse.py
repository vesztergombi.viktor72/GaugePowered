# -*- coding: utf-8 -*-
"""Test the steam parsing utilities.
"""
import datetime
from decimal import Decimal
import unittest
import os
from lxml import etree

from gamestradamus.channel.steam import parse

base_dir = os.path.dirname(os.path.abspath(__file__))


#-------------------------------------------------------------------------------
class TestParsingGamePage(unittest.TestCase):

    #--------------------------------------------------------------------------
    def _test_game_page(self, region, path, sale_price, price, is_subscription):
        contents = open(os.path.join(base_dir, "steam-data", path), "r").read()
        game = parse.game_page(contents, region)
        self.assertEqual(game['price'], price)
        self.assertEqual(game['sale_price'], sale_price)
        self.assertEqual(game['is_subscription'], is_subscription)

    #--------------------------------------------------------------------------
    def test_orion_dino_horde(self):
        self._test_game_page(
                "us", "orion-dino-horde-1.html", Decimal("3.74"), Decimal("14.99"), False
            )

    #--------------------------------------------------------------------------
    def test_hate_plus(self):
        self._test_game_page(
                "us", "hate-plus-1.html", Decimal("8.99"), Decimal("9.99"), False
            )

    #--------------------------------------------------------------------------
    def test_the_cave(self):
        self._test_game_page(
                "us", "the-cave-1.html", Decimal("14.99"), Decimal("14.99"), False
            )

    #--------------------------------------------------------------------------
    def test_fallout_2(self):
        self._test_game_page(
                "us", "fallout-2-1.html", Decimal("2.49"), Decimal("9.99"), False
            )

    #--------------------------------------------------------------------------
    def test_dont_starve(self):
        self._test_game_page(
                "us", "dont-starve-1.html", Decimal("7.49"), Decimal("14.99"), False
            )

    #--------------------------------------------------------------------------
    def test_red_orchestra_ostfront_41_45(self):
        self._test_game_page(
                "us", "red-orchestra-ostfront-41-45-1-.html", Decimal("9.99"), Decimal("9.99"), False
            )

    #--------------------------------------------------------------------------
    def test_electronic_super_joy(self):
        self._test_game_page(
                "us", "electronic-super-joy-1.html", Decimal("7.99"), Decimal("7.99"), False
            )

    #--------------------------------------------------------------------------
    def test_borderlands_2(self):
        self._test_game_page(
                "us", "borderlands-2-1.html", Decimal("29.99"), Decimal("29.99"), False
            )

    #--------------------------------------------------------------------------
    def test_sir_you_are_being_hunted(self):
        self._test_game_page(
                "us", "sir-you-are-being-hunted-1.html", Decimal("19.99"), Decimal("19.99"), False
            )

    #--------------------------------------------------------------------------
    def test_the_witcher_2(self):
        self._test_game_page(
                "us", "the-witcher-2-1.html", Decimal("4.99"), Decimal("19.99"), False
            )

    #--------------------------------------------------------------------------
    def test_darkfall(self):
        self._test_game_page(
                "us", "darkfall.html", Decimal("0.0"), Decimal("0.0"), True
            )

    #--------------------------------------------------------------------------
    def test_deadisland_epidemic(self):
        self._test_game_page(
                "us", "deadisland-epidemic.html", Decimal("16.99"), Decimal("16.99"), False
            )

    #--------------------------------------------------------------------------
    def test_deadisland_patient_zero(self):
        self._test_game_page(
                "us", "deadisland-patient-zero.html", Decimal("16.99"), Decimal("16.99"), False
            )

    #--------------------------------------------------------------------------
    def test_deadisland_contagion(self):
        self._test_game_page(
                "us", "deadisland-contagion.html", Decimal("44.99"), Decimal("44.99"), False
            )

    #--------------------------------------------------------------------------
    def test_warhammer_unavailable(self):
        file = os.path.join(base_dir, "steam-data", "warhammer-unavailable.html")
        contents = open(file, "r").read()
        game = parse.game_page(contents, "us")
        self.assertEqual(game['game_page_loaded'], False)
        self.assertEqual(game['available_in_region'], False)

    #--------------------------------------------------------------------------
    def test_project_nimbus(self):
        file = os.path.join(base_dir, "steam-data", "project_nimbus.html")
        contents = open(file, "r").read()
        game = parse.game_page(contents, "us")
        self.assertEqual(game['game_page_loaded'], True)
        self.assertEqual(game['no_price'], True)

    #--------------------------------------------------------------------------
    def test_solarix(self):
        file = os.path.join(base_dir, "steam-data", "solarix.html")
        contents = open(file, "r").read()
        game = parse.game_page(contents, "us")
        self.assertEqual(game['game_page_loaded'], True)
        self.assertEqual(game['no_price'], True)

    #--------------------------------------------------------------------------
    def test_au_rectifier(self):
        file = os.path.join(base_dir, "steam-data", "au-rectifier.html")
        contents = open(file, "r").read()
        game = parse.game_page(contents, "au")
        self.assertEqual(game['game_page_loaded'], True)
        self.assertEqual(game['no_price'], True)

    #--------------------------------------------------------------------------
    def test_au_space_siege(self):
        file = os.path.join(base_dir, "steam-data", "au-space-siege.html")
        contents = open(file, "r").read()
        game = parse.game_page(contents, "au")
        self.assertEqual(game['game_page_loaded'], True)
        self.assertEqual(game['no_price'], True)

    #--------------------------------------------------------------------------
    def test_br_day_z(self):
        self._test_game_page(
                "br", "br-dayz.html", Decimal("77.99"), Decimal("77.99"), False
            )

#-------------------------------------------------------------------------------
class TestParsingSteamSearchPage(unittest.TestCase):

    #--------------------------------------------------------------------------
    def test_search_page_with_sales_us_region(self):
        file = os.path.join(base_dir, "steam-data", "us-sale-page-with-sales.html")
        contents = open(file, "r").read()
        parser = etree.HTMLParser()
        doc = etree.fromstring(contents, parser)
        apps = parse.get_apps_from_search_results(doc, "us")
        self.assertEqual(
            apps,
            [
                {
                    'release': datetime.datetime(2010, 10, 11, 0, 0),
                    'name': u'Governor of Poker 2 - Premium Edition',
                    'genre': 'Adventure, Casual, Simulation',
                    'sale_price': Decimal('19.99'),
                    'price': Decimal('19.99'),
                    'app_id': 70210,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2010, 10, 11, 0, 0),
                    'name': u'Governor of Poker 2',
                    'genre': 'Adventure, Casual, Simulation',
                    'sale_price': Decimal('9.99'),
                    'price': Decimal('9.99'),
                    'app_id': 70200,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2009, 12, 2, 0, 0),
                    'name': u'Gothic\xae 3',
                    'genre': 'Action, RPG',
                    'sale_price': Decimal('9.99'),
                    'price': Decimal('9.99'),
                    'app_id': 39500,
                    'metascore': 63
                },
                {
                    'release': datetime.datetime(2010, 11, 5, 0, 0),
                    'name': u'Gothic II: Gold Edition',
                    'genre': 'Action, RPG',
                    'sale_price': Decimal('9.99'),
                    'price': Decimal('9.99'),
                    'app_id': 39510,
                    'metascore': 79
                },
                {
                    'release': datetime.datetime(2011, 3, 22, 0, 0),
                    'name': u'Gothic 3: Forsaken Gods Enhanced Edition',
                    'genre': 'RPG',
                    'sale_price': Decimal('9.99'),
                    'price': Decimal('9.99'),
                    'app_id': 65600,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2010, 11, 5, 0, 0),
                    'name': u'Gothic 1',
                    'genre': 'Action, RPG',
                    'sale_price': Decimal('19.99'),
                    'price': Decimal('19.99'),
                    'app_id': 65540,
                    'metascore': 81
                },
                {
                    'release': datetime.datetime(2012, 8, 30, 0, 0),
                    'name': u'Gotham City Impostors Free to Play',
                    'genre': 'Action, Free to Play',
                    'sale_price': Decimal('0.00'),
                    'price': Decimal('0.00'),
                    'app_id': 206210,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2013, 9, 27, 0, 0),
                    'name': u'Gorky 17',
                    'genre': 'RPG, Strategy',
                    'sale_price': Decimal('4.99'),
                    'price': Decimal('4.99'),
                    'app_id': 253920,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2013, 10, 17, 0, 0),
                    'name': u'Goodbye Deponia',
                    'genre': 'Adventure, Indie',
                    'sale_price': Decimal('19.99'),
                    'price': Decimal('19.99'),
                    'app_id': 241910,
                    'metascore': 80
                },
                {
                    'release': datetime.datetime(2013, 8, 15, 0, 0),
                    'name': u'Gone Home',
                    'genre': 'Adventure, Indie',
                    'sale_price': Decimal('19.99'),
                    'price': Decimal('19.99'),
                    'app_id': 232430,
                    'metascore': 86
                },
                {
                    'release': datetime.datetime(2013, 12, 6, 0, 0),
                    'name': u'Gomo',
                    'genre': 'Adventure, Indie',
                    'sale_price': Decimal('2.71'),
                    'price': Decimal('7.99'),
                    'app_id': 265330,
                    'metascore': 50,
                },
                {
                    'release': datetime.datetime(2010, 10, 26, 0, 0),
                    'name': u'Golden Axe\u2122 II',
                    'genre': 'Action',
                    'sale_price': Decimal('2.99'),
                    'price': Decimal('2.99'),
                    'app_id': 71112,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2010, 6, 1, 0, 0),
                    'name': u'Golden Axe\u2122',
                    'genre': 'Action',
                    'sale_price': Decimal('2.99'),
                    'price': Decimal('2.99'),
                    'app_id': 34276,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2012, 5, 2, 0, 0),
                    'name': u'Golden Axe III',
                    'genre': None,
                    'sale_price': Decimal('2.99'),
                    'price': Decimal('2.99'),
                    'app_id': 211202,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2014, 7, 25, 0, 0),
                    'name': u'Gold Rush! - Classic',
                    'genre': 'Action, Adventure',
                    'sale_price': Decimal('0.00'),
                    'price': Decimal('0.00'),
                    'app_id': 308000,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2013, 9, 13, 0, 0),
                    'name': u'Godus',
                    'genre': 'Indie, Simulation, Strategy, Early Access',
                    'sale_price': Decimal('19.99'),
                    'price': Decimal('19.99'),
                    'app_id': 232810,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2014, 7, 24, 0, 0),
                    'name': u'Gods Will Be Watching',
                    'genre': 'Adventure, Indie',
                    'sale_price': Decimal('8.99'),
                    'price': Decimal('9.99'),
                    'app_id': 274290,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2013, 4, 19, 0, 0),
                    'name': u'God Mode',
                    'genre': 'Action',
                    'sale_price': Decimal('9.99'),
                    'price': Decimal('9.99'),
                    'app_id': 227480,
                    'metascore': 58
                },
                {
                    'release': datetime.datetime(2014, 4, 1, 0, 0),
                    'name': u'Goat Simulator',
                    'genre': 'Casual, Indie, Simulation',
                    'sale_price': Decimal('5.99'),
                    'price': Decimal('9.99'),
                    'app_id': 265930,
                    'metascore': 62
                },
                {
                    'release': datetime.datetime(2014, 2, 21, 0, 0),
                    'name': u'Go! Go! Nippon! ~My First Trip to Japan~',
                    'genre': 'Adventure',
                    'sale_price': Decimal('9.95'),
                    'price': Decimal('9.95'),
                    'app_id': 251870,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2013, 3, 14, 0, 0),
                    'name': u'Go Home Dinosaurs!',
                    'genre': 'Strategy, Indie, Casual',
                    'sale_price': Decimal('9.99'),
                    'price': Decimal('9.99'),
                    'app_id': 216090,
                    'metascore': None
                },
                {
                    'release': None,
                    'name': u'Gnomoria',
                    'genre': 'Strategy, Indie, Simulation, Early Access',
                    'sale_price': Decimal('7.99'),
                    'price': Decimal('7.99'),
                    'app_id': 224500,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2011, 9, 30, 0, 0),
                    'name': u'Glowfish',
                    'genre': 'Adventure, Casual, Indie',
                    'sale_price': Decimal('9.99'),
                    'price': Decimal('9.99'),
                    'app_id': 60350,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2014, 6, 9, 0, 0),
                    'name': u'Global Outbreak: Doomsday Edition',
                    'genre': 'Action, Indie, Strategy, Early Access',
                    'sale_price': Decimal('12.99'),
                    'price': Decimal('12.99'),
                    'app_id': 264260,
                    'metascore': None
                },
                {
                    'release': datetime.datetime(2011, 10, 26, 0, 0),
                    'name': u'Global Ops: Commando Libya',
                    'genre': 'Action',
                    'sale_price': Decimal('14.99'),
                    'price': Decimal('14.99'),
                    'app_id': 200020,
                    'metascore': 37
                }
            ]
        )
