import datetime
import json
import decimal
import sys
import traceback

from bs4 import BeautifulSoup
from gamestradamus.regions import REGIONS
from lxml import etree

#-------------------------------------------------------------------------------
class InvalidPrice(Exception):
    """Prices from the spider were invalide"""

#-------------------------------------------------------------------------------
def gather_games_from_user_page(text):
    start_pattern = "var rgGames = "
    end_pattern = "}];"
    start_pos = text.find(start_pattern)
    end_pos = text.find(end_pattern, start_pos)
    if -1 in [start_pos, end_pos]:
        return

    game_list = json.loads(text[start_pos+len(start_pattern):end_pos+len(end_pattern)-1])
    for game in game_list:
        store_url = "http://store.steampowered.com/app/%s/" % (game['appid'],)
        hours_played = unicode(game.get('hours_forever', '0.0'))
        hours_played = hours_played.replace(",", "")
        hours_played = decimal.Decimal(hours_played)

        friendly_url = game.get('friendlyURL', False)
        if not friendly_url:
            friendly_url = None

        achievements = game['availStatLinks'].get('achievements', False)

        yield {
            "app_id": game['appid'],
            "name": game['name'],
            "logo": game['logo'],
            "hours_played": hours_played,
            "store_url": store_url,
            "friendly_url": friendly_url,
            "achievements": achievements,
        }

#-------------------------------------------------------------------------------
def gather_achievements_from_stats_page(html):
    """Return a list of achievements held by a user for a specific game."""

    soup = BeautifulSoup(html, "html.parser") 

    # There are two different HTML structures for achievement pages.
    summary = soup.find('div', attrs={'id': 'topSummaryAchievements'})
    if summary is not None:
        summary = ''.join(summary.findAll(text=True, recursive=False))
    else:
        # The count wasn't in the first format, try the second.
        summary = soup.find('div', attrs={'class': 'achievementStatusText'})
        if summary is not None:
            summary = ''.join(summary.findAll(text=True, recursive=False))
        else:
            # We don't know where the count is, default to zero.
            summary = '0 of 0'

    limit = int(summary.split(' of ')[0][-3:])
    if limit < 1:
        return 

    for achievement in soup.findAll('div', 'achieveTxt', limit=limit):
        name = achievement.find('h3')
        if name is not None:
            name = u''.join(name.findAll(text=True))
        else:
            continue

        description = achievement.find('h5')
        if description is not None:
            description = u''.join(description.findAll(text=True))
        else:
            description = u''

        yield {
            'name': name,
            'description': description,
        }

#-------------------------------------------------------------------------------
def get_last_search_page(doc):

    pagination = doc.xpath("//*[@id='search_result_container']//*[@class='search_pagination_right']//a")
    max_page = -1
    for a in pagination:
        try:
            page = int(a.text)
            max_page = max(page, max_page)
        except (ValueError, TypeError):
            continue
    return max_page

#-------------------------------------------------------------------------------
# Precompile the xpath selectors.
search_anchor_finder = etree.XPath("//*[@id='search_results']//a[contains(concat(' ', normalize-space(@class), ' '), ' search_result_row ')]")
search_name_finder = etree.XPath(".//div[contains(concat(' ', normalize-space(@class), ' '), ' search_name ')]//span")
price_finder = etree.XPath(".//div[contains(concat(' ', normalize-space(@class), ' '), ' search_price ')]//text()")
genre_finder = etree.XPath(".//div[contains(concat(' ', normalize-space(@class), ' '), ' search_name ')]//p//text()")
release_finder = etree.XPath(".//div[contains(concat(' ', normalize-space(@class), ' '), ' search_released ')]")
def get_apps_from_search_results(doc, region):
    pricing = REGIONS[region]
    currency_symbol = pricing['numbers'].dollar_symbol

    anchors = search_anchor_finder(doc)

    apps = []
    for a in anchors:
        app = {}

        # Extract the AppID
        app_id = a.get('href')
        app_id = app_id.replace('http://store.steampowered.com/app/', '')
        app_id = app_id.split('/')[0]

        try:
            app_id = int(app_id)
        except (ValueError, TypeError):
            continue

        app.update({'app_id': app_id})

        # Extract the price(s)
        price = decimal.Decimal('0.00')
        sale_price = decimal.Decimal('0.00')

        prices = ''.join(price_finder(a))
        prices = [p.strip() for p in prices.split(currency_symbol) if p.strip()]

        if len(prices) == 2:
            try:
                price = decimal.Decimal(prices[0].replace(",", "."))
                sale_price = decimal.Decimal(prices[1].replace(",", "."))
            except (ValueError, TypeError, decimal.InvalidOperation, IndexError), e:
                price = decimal.Decimal('0.00')
                sale_price = decimal.Decimal('0.00')
        elif len(prices) == 1:
            try:
                price = decimal.Decimal(prices[0].replace(",", "."))
            except (ValueError, TypeError, decimal.InvalidOperation, IndexError), e:
                price = decimal.Decimal('0.00')
            sale_price = price

        app.update({'price': price})
        app.update({'sale_price': sale_price})

        # Extract the name
        app.update({
            'name': u''.join([e.text or '' for e in search_name_finder(a)]).strip()
        })

        # Extract the genre
        genre = ''.join(genre_finder(a)).replace('\r', '').replace('\n', '').replace('\t', '')
        genre = genre.split(' - ')
        if len(genre) > 1:
            app.update({'genre': genre[0].strip(' \n\t')})
        else:
            if ': ' not in genre[0] and genre[0]:
                app.update({'genre': genre[0]})
            else:
                app.update({'genre': None})

        # Release date.
        release_text = release_finder(a)[0].text or ''
        release = release_text.strip('\n').strip('\t')
        try:
            release = datetime.datetime.strptime(release, '%d %b %Y')
        except ValueError, e:
            try:
                release = datetime.datetime.strptime(release, '%b %d, %Y')
            except ValueError, e:
                try:
                    release = datetime.datetime.strptime(release, '%d %Y')
                except ValueError, e:
                    try:
                        release = datetime.datetime.strptime(release, '%Y')
                    except ValueError, e:
                        release = None
        app.update({
            'release': release
        })

        apps.append(app)
    return apps

#-------------------------------------------------------------------------------
def parse_price(html_price, region):
    price = html_price.strip()

    # Steam currently uses USD for some countries
    # which means that in addition to the dollar symbol for that
    # country, it may also include " USD". Strip that here.
    price = price.replace(" USD", "")

    # Strip whitespace
    price = price.strip()

    # Strip all currency symbols
    settings = REGIONS[region]
    price = price.replace(settings['numbers'].dollar_symbol_no_html, "")

    # replace thousands separator with nothing.
    price = price.replace(settings['numbers'].thousands_separator, '')

    # replace radix with dots. 
    price = price.replace(settings['numbers'].radix, '.')

    try:
        price = decimal.Decimal(price)
    except (ValueError, TypeError, decimal.InvalidOperation, IndexError), e:
        raise InvalidPrice("%s isn't a valid price" % (html_price,))
    return price

#-------------------------------------------------------------------------------
def game_page(html, region):
    soup = BeautifulSoup(html, "html.parser")

    price_info = {
        'game_page_loaded': False,
        'price': decimal.Decimal("0.00"),
        'sale_price': decimal.Decimal("0.00"),
        'is_subscription': False,
        'available_in_region': False,
        'no_price': False,
    }

    try:
        # Check to see if it's available in the region.
        error = soup.select('span.error')
        if error and error[0].string == 'This item is currently unavailable in your region':
            return price_info

        # this check ensures that we're on the game page.
        name = soup.select(".apphub_AppName")
        if name:
            price_info['game_page_loaded'] = True
        else:
            return price_info

        price_info['available_in_region'] = True

        game_purchase_areas = soup.select("div.game_area_purchase_game")
        if not game_purchase_areas:
            price_info['no_price'] = True
            return price_info

        area = game_purchase_areas[0]

        sale_price_elements = area.select(".discount_final_price")
        if len(sale_price_elements) > 0:
            price_info['sale_price'] = parse_price(sale_price_elements[0].text, region)
            price_elements = area.select(".discount_original_price")
            price_info['price'] = parse_price(price_elements[0].text, region)
        else:
            price_elements = area.select(".game_purchase_price.price")
            if price_elements:
                price_info['sale_price'] = price_info['price'] = parse_price(price_elements[0].text, region)
            else:
                # if we don't have a price for purchaseing the game, but we DO
                # have a price for subscribing to the game, then set the prices
                subscriptions_menu = area.select(".game_purchase_subscription")
                if subscriptions_menu:
                    price_info['is_subscription'] = True
    except IndexError, e:
        raise InvalidPrice("%s" % traceback.format_exc(e))

    return price_info

#-------------------------------------------------------------------------------
def get_agecheck_url(html):
    soup = BeautifulSoup(html, "html.parser")
    form = soup.select("div#agegate_box form")
    if form:
        form = form[0]
        snr = soup.select("div#agegate_box form input")[0]
        return form['action'], snr['value']
    return None, None

