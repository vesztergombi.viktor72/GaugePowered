from __future__ import absolute_import

import decimal
import requests
from bs4 import BeautifulSoup

import hydro
import plumbing.auth

from gamestradamus import cli, timeutils
from gamestradamus.channel.steam import parse
from gamestradamus import units


#---------------------------------------------------------------------------
PRIVATE_PROFILE = """<div class="profile_private_info">"""
#---------------------------------------------------------------------------
def update_hours_in_game(steam_id_64, store=None):
    raise Exception('This method is deprecated. Use the async task.')

