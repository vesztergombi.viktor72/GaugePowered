from __future__ import absolute_import

import random
import decimal
import json
import requests
import traceback
from collections import defaultdict
from bs4 import BeautifulSoup

from gamestradamus import cli, email, timeutils, units
from gamestradamus.channel import steam
from gamestradamus.crystalball import predictions
from gamestradamus.taskqueue.celery import celery
from gamestradamus.channel.steam import parse
from gamestradamus.regions import REGIONS

from plumbing.auth.units import UserAccount

"""
Task Chains:

    -> welcome
        -> steam_update_hours_in_game (blocking)
            -> steam_update_achievements_in_game (async)

    -> steam_update_hours_in_game
        -> steam_update_achievements_in_game (async)

    -> update_game_price
        -> notify_game_watches (async)

    -> update_game_statistics
        -> notify_game_watches (async)

"""

#-------------------------------------------------------------------------------
import logging
requests_log = logging.getLogger("requests")
requests_log.setLevel(logging.WARNING)

#-------------------------------------------------------------------------------
PRIVATE_PROFILE = """<div class="profile_private_info">"""
#-------------------------------------------------------------------------------
@celery.task
def steam_update_hours_in_game(steam_id_64):

    store = cli.storage_manager()
    sandbox = store.new_sandbox()

    # We can't update a user_account record that we haven't parsed before.
    user_account = sandbox.unit(
        UserAccount,
        openid='http://steamcommunity.com/openid/id/%s' % steam_id_64
    )
    if not user_account:
        raise Exception("Can\'t recall user_account with steam_id_64 of '%s'" % steam_id_64)

    # Ensure that we can mark the last time we updated this accounts games. 
    user_attrs = user_account.UserAccountAttributes()
    if user_attrs is None:
        return

    stats = user_account.UserAccountStatistics()
    if stats is None:
        return

    # Get the steam nickname for an account.
    try:
        url = 'http://steamcommunity.com/profiles/%s/games?tab=all' % steam_id_64
        r = requests.get(url, timeout=30.0)
        r.raise_for_status()

        if PRIVATE_PROFILE in r.text:
            user_attrs.private_profile_since = timeutils.utcnow()
            user_attrs.games_last_updated = timeutils.utcnow()
            sandbox.flush_all()
            return

        soup = BeautifulSoup(r.text)
        nickname = unicode(soup.findAll('title')[0].contents[0].split(' :: ')[1])
    except Exception, e:
        traceback.print_exc()
        return

    user_account.nickname = nickname

    # Account statistics
    statistics = {
        'game_count': 0,
        'hours_played': decimal.Decimal('0.0')
    }

    # Process the game list and make records against the user profile.
    user_account_games = user_account.UserAccountGame()
    user_account_games = dict([(uag.game_id, uag) for uag in user_account_games])

    games = parse.gather_games_from_user_page(r.text)
    for g in games:
        game = sandbox.Game(**{
            'app_id': g['app_id']
        })
        if game is None:
            game = units.Game(**{
                'app_id': g['app_id'],
                'name': g['name'],
                'logo': g['logo'],
            })
            sandbox.memorize(game)

        # Relate the game to the UserAccount or update the information.
        uag = user_account_games.pop(game.id, None)
        if uag is None:
            # they don't have this game in their library directly, but check for
            # a canonical version:
            uag = user_account_games.pop(game.canonical_game_id, None)
            if uag is None:
                uag = units.UserAccountGame(**{
                    'game_id': game.id,
                    'user_account_id': user_account.id,
                    'corrected': False,
                })
                sandbox.memorize(uag)

        # Update the dollars paid if the record hasn't been corrected.
        region = user_attrs.region
        uag.set_initial_price(region, game=game)

        # Update the account achievements. In the background. 
        # if g['achievements'] and g['hours_played'] and g['hours_played'] != uag.steam_hours_played:
        #    steam_update_achievements_in_game.apply_async(**{
        #        'args': [steam_id_64, game.id, g.get('friendly_url')],
        #        'routing_key': 'tasks.background.update-achievements-in-game'
        #    })

        # Update the hours from steam.
        if g['hours_played']:
            uag.steam_hours_played = g['hours_played']
            uag.update_cost_per_hour()

        statistics['game_count'] += 1
        statistics['hours_played'] += g['hours_played']

    # Free play weekends, games that have been removed from an account, etc
    # will no longer have a record on Steam, so remove our data if nothing has been added to it locally.
    for k, uag in user_account_games.iteritems():
        if uag.added_by_user:
            continue
        if uag.finished:
            continue
        if uag.corrected:
            continue
        if uag.ignore:
            continue
        if uag.tags:
            continue
        if uag.rating:
            continue
        if ((uag.steam_hours_played or 0) + (uag.custom_hours_played or 0)) == 0:
            uag.forget()

    # Mark the game update task as being complete.
    user_attrs.games_last_updated = timeutils.utcnow()

    # If we get this far, we know they don't have a private profile.
    # mark it as such.
    user_attrs.private_profile_since = None

    # Update the cached statistics about this account.
    stats.game_count = statistics['game_count']
    stats.hours_played = statistics['hours_played']

    sandbox.flush_all()
    store.shutdown()


#-------------------------------------------------------------------------------
@celery.task
def steam_update_achievements_in_game(steam_id_64, game_id, friendly_url):
    """Update the achievements that a user has for a game.""" 
     
    store = cli.storage_manager()
    sandbox = store.new_sandbox()

    user_account = sandbox.UserAccount(**{
        'openid': 'http://steamcommunity.com/openid/id/%s' % steam_id_64
    })
    if user_account is None:
        return

    game = sandbox.Game(id=game_id)
    if game is None:
        return

    stats = user_account.UserAccountStatistics()
    if stats is None:
        return

    user_account_achievements = user_account.UserAccountAchievement()
    user_account_achievements = dict(
        [(uaa.achievement_id, uaa) for uaa in user_account_achievements]
    )

    try:
        # url = 'http://steamcommunity.com/profiles/%s/appid/%s/stats/achievements'
        url = 'http://steamcommunity.com/profiles/%s/stats/%s?tab=achievements' % (
            steam_id_64, friendly_url
        )
        r = requests.get(url, timeout=30.0)
        r.raise_for_status()

        for a in parse.gather_achievements_from_stats_page(r.text):
            achievement = sandbox.Achievement(**{
                'name': a.get('name'),
                'game_id': game.id,
            })
            if achievement is None:
                achievement = units.Achievement(**{
                    'game_id': game.id,
                    'name': a.get('name'),
                    'description': a.get('description'),
                })
                sandbox.memorize(achievement)

            uaa = user_account_achievements.pop(achievement.id, None)
            if uaa is None:
                uaa = units.UserAccountAchievement(**{
                    'achievement_id': achievement.id,
                    'user_account_id': user_account.id
                })
                sandbox.memorize(uaa)
        
        stats.achievement_count = sandbox.count(
            units.UserAccountAchievement,
            lambda x: x.user_account_id == user_account.id
        )

    except Exception, e:
        traceback.print_exc()
        return

    sandbox.flush_all()
    store.shutdown()

#-------------------------------------------------------------------------------
@celery.task
def predictions_update_family_tree(link, steam_id_64, count):
    predictions.update_family_tree(steam_id_64, count)

#-------------------------------------------------------------------------------
@celery.task
def predictions_prophesy(link, steam_id_64, count):
    predictions.prophesy(steam_id_64, count)

#-------------------------------------------------------------------------------
@celery.task
def welcome(steam_id_64, relative_count, prediction_count):
    """The number of new sandboxes that are created in this method is not an error.
    
    Because the individual tasks flush the sandboxes and update the user_attrs
    so often, we need to make sure we are capturing the latest version of the user_attrs
    at each step.
    """

    store = cli.storage_manager()
    sandbox = store.new_sandbox()
    user_account = sandbox.UserAccount(**{
        'openid': 'http://steamcommunity.com/openid/id/%s' % steam_id_64
    })
    if user_account is None:
        return

    # Attempt to pull the account data from Steam.
    steam_update_hours_in_game(steam_id_64)

    # Check if the account is now a private profile?
    sandbox = store.new_sandbox()
    user_attrs = sandbox.UserAccountAttributes(**{'user_account_id': user_account.id})
    if user_attrs.private_profile_since:
        # We need to re-welcome them if their profile is private. 
        user_attrs.welcome_started = None
        user_attrs.welcomed_since = None
        sandbox.flush_all()

    else:
        sandbox = store.new_sandbox()
        user_attrs = sandbox.UserAccountAttributes(**{'user_account_id': user_account.id})
        user_attrs.welcomed_since = timeutils.utcnow()
        sandbox.flush_all()

    store.shutdown()

#-------------------------------------------------------------------------------
@celery.task
def notify_game_watches(game_id, region, operations=None):
    """Notify all of the satisfied game watches for a game.

    Optionally limit the watch operation to a specific set.
    """
    store = cli.storage_manager()
    sandbox = store.new_sandbox()

    # get the game
    game = sandbox.Game(id=game_id)
    if game is None:
        return

    if not operations:
        operations = ['price', 'cost-per-hour']

    # TODO: we could optimize this to only select matching watches
    #       but we can still do that later.
    watches = sandbox.recall(
        units.UserAccountGameWatch << UserAccount << units.UserAccountAttributes,
        lambda uag, ua, attrs: uag.game_id == game_id and attrs.region == region
    )

    for watch, user_account, attrs in watches:
        if not watch.satisfied(game, region):
            continue
        if watch.operation not in operations:
            continue

        # Now we notify the user.
        context = {
            'user_account': user_account,
            'game': game,
            'watch': watch,
            'region': region,
        }

        # Subscribed emails:
        email_addresses = sandbox.recall(
            units.UserAccountEmail,
            lambda uag: (
                uag.user_account_id==watch.user_account_id and
                uag.date_confirmed is not None and
                uag.watch_list_notifications==True
            )
        )
        for to_address in email_addresses:
            email.render_add_to_email_queue(**{
                'to_address': to_address.email,
                'email_name': 'notification-watch-list',
                'context': context,
                'options': '{ "tags": ["notifications-watch-list"] }',
                'sandbox': sandbox,
            })

    sandbox.flush_all()
    store.shutdown()

#-------------------------------------------------------------------------------
@celery.task
def update_game_price(game_id, region):
    """Update the game price
    """
    from gamestradamus.channel.steam.spider import AgeCheckSpider
    pricing = REGIONS[region]
    price_attr = pricing['game_attributes']['price']
    sale_price_attr = pricing['game_attributes']['sale_price']
    available_since_attr = pricing['game_attributes']['available_since']
    on_sale_since_attr = pricing['game_attributes']['on_sale_since']
    country_abbreviation = pricing['country_abbreviation']

    store = cli.storage_manager()
    sandbox = store.new_sandbox()

    # get the game
    attributes_to_set = {}
    def get_game(id):
        """Use a view to get a game and return a named dict"""
        x = sandbox.store.view((
            units.Game,
            lambda g: (
                g.id,
                g.app_id,
                getattr(g, price_attr),
                getattr(g, sale_price_attr),
                getattr(g, on_sale_since_attr),
            ),
            lambda g: g.id == id
        ))
        if len(x) < 1:
            return None
        return {
            'id': x[0][0],
            'app_id': x[0][1],
            price_attr: x[0][2],
            sale_price_attr: x[0][3],
            on_sale_since_attr: x[0][4],
        }
    game = get_game(game_id)
    if game is None:
        return

    game_sale_price = game[sale_price_attr]
    game_price = game[price_attr]

    # Get the game url data and get around the age check.
    url = "http://store.steampowered.com/app/%s/?cc=%s" % (game['app_id'], country_abbreviation)
    delay_between_requests = 1.0 / 5.0
    spider = AgeCheckSpider(delay_between_requests)
    response = spider.GET(url)
    if response.url == "http://store.steampowered.com/":
        # If this game redirects us back to the home page, then there isn't much
        # we can do for it.
        return

    age_check = spider.check_response(url, response)
    if age_check:
        response = age_check
    game_info = parse.game_page(response.text, region)

    if not game_info['game_page_loaded']:
        # If the game page didn't load - we can't do anything
        return

    if game_info['no_price']:
        # If the game doesn't have a price then we'll just ignore
        # it this time around and hope it's fixed the next time we
        # parse it.
        return

    if game_info['is_subscription']:
        # Subscription games don't have a predictable cost.
        attributes_to_set['predictable_cost'] = False

    # Now use the pricing information to see if we need to update
    # the game and/or notify anyone that is watching.
    notify_watches = False

    if game_info['sale_price'] < game_sale_price:
        # When a game that is already on sale gets a larger discount,
        # like flash or daily sales.
        notify_watches = True

        # When a game has its base price dropped, we want to have watches sent out,
        # but the game shouldn't be marked as on sale.
        if game_info['sale_price'] < game_info['price']:
            attributes_to_set[on_sale_since_attr] = timeutils.utcnow()

    elif game_info['sale_price'] < game_info['price'] and not game[on_sale_since_attr]:
        # When a game is put on sale
        attributes_to_set[on_sale_since_attr] = timeutils.utcnow()
        notify_watches = True

    if game[on_sale_since_attr] and not game_info['sale_price'] < game_info['price']:
        # When a game is taken off sale.
        attributes_to_set[on_sale_since_attr] = None
        notify_watches = False


    attributes_to_set[price_attr] = game_info['price']
    attributes_to_set[sale_price_attr] = game_info['sale_price']
    attributes_to_set[available_since_attr] = timeutils.utcnow()

    if attributes_to_set:
        attributes_to_set['id'] = game['id']
        sandbox.store.schema['Game'].save(**attributes_to_set)
    sandbox.flush_all()

    if notify_watches:
        print "Notifying watches about %s for price %s vs %s" % (
                game['id'],
                game_info['sale_price'],
                game_info['price'],
            )
        notify_game_watches.apply_async(**{
            'args': [game['id'], region],
            'routing_key': 'tasks.premium.notify-watches',
        })

    store.shutdown()

#-------------------------------------------------------------------------------
@celery.task
def update_game_statistics(game_id):
    """Calculate community wide statistics for an AppID"""

    store = cli.storage_manager()
    sandbox = store.new_sandbox()
    primary_game = sandbox.Game(**{'id': game_id})
    if primary_game is None:
        return

    zero = decimal.Decimal('0.00')
    game_set = [primary_game]
    if primary_game.stat_set_id:
        game_set = sandbox.recall(
                units.Game,
                lambda x: x.stat_set_id == primary_game.stat_set_id
            )
    game_ids = [str(a.id) for a in game_set]

    def set_statistic(attribute, value):
        for game in game_set:
            setattr(game, attribute, value)

    print 'Processing AppID: %s, StatSet: %s' % (primary_game.app_id, primary_game.stat_set_id)
    rows, cols = sandbox.store.db.fetch("""
        SELECT
            steam_hours_played AS hours_played
        FROM "UserAccountGame"
        WHERE game_id IN (%s)
            AND steam_hours_played >= 0.5
            AND steam_hours_played <= 8766
        ORDER BY steam_hours_played""" % (','.join(game_ids))
    )

    # Trim any sample that is above 95%.
    if len(rows) > 0:
        upper = rows[int(len(rows) * 0.95)][0]
        rows = [x for x in rows if x[0] <= upper]

    # calculate the average playtime.
    hours_played = decimal.Decimal('0.00')
    for row in rows:
        hours_played = hours_played + row[0]
    if len(rows):
        set_statistic("average_hours_played", hours_played / len(rows))
    else:
        set_statistic("average_hours_played", hours_played)

    # calculate the median playtime.
    count = len(rows)
    hours_played = decimal.Decimal('0.00')
    if len(rows) > 0:
        if count % 2:
            hours_played = rows[count / 2][0]
        else:
            hours_played = (rows[count / 2 - 1][0] + rows[count / 2][0]) / 2

    if (primary_game.median_hours_played or zero) < hours_played:
        # Update watches for this game if the predicted hours increase.
        # Only notify watches that are for cost-per-hour, not price.
        for game in game_set:
            for region_settings in REGIONS.values():
                notify_game_watches.apply_async(**{
                    'args': (game.id, region_settings['value'], ['cost-per-hour']),
                    'routing_key': 'tasks.premium.notify-watches'
                })
    set_statistic("median_hours_played", hours_played)
    set_statistic("owner_count", count)

    #---------------------------------------------------------------------
    # CALCULATE A BAR CHART OF THE HOURS PLAYED.
    class FormattedDecimal(float):
        def __repr__(self):
            return u'%.2f' % self

    class DecimalEncoder(json.JSONEncoder):
        def default(self, obj):
            if type(obj) == decimal.Decimal:
                return FormattedDecimal(obj)
            return json.JSONEncoder.default(self, obj) 

    if len(rows) > 0:
        minimum = decimal.Decimal("%.2f" % rows[0][0])
        maximum = decimal.Decimal("%.2f" % rows[len(rows) - 1][0])
        step = ((maximum - minimum) / 40).quantize(decimal.Decimal('.001'))

        counts = defaultdict(int)
        if step:
            for x in rows:
                bin = int((x[0] - minimum) / step)
                counts[bin] += 1

        export = []
        for hours, count in counts.iteritems():
            export.append({
                'c': count,
                'h': minimum + hours * step
            })

        if len(export) > 0:
            set_statistic(
                "hours_played_distribution",
                json.dumps(export, cls=DecimalEncoder)
            )
        else:
            set_statistic("hours_played_distribution", None)

    #----------------------------------------------------------------
    rows, cols = sandbox.store.db.fetch("""
        SELECT SUM("UserAccountGame".rating) AS rating,
            COUNT("UserAccountGame".id) AS count
        FROM "UserAccountGame"
        WHERE "UserAccountGame".game_id IN (%s)
            AND "UserAccountGame".rating > 0""" % (','.join(game_ids))
    )
    result = rows[0]

    set_statistic("rating", (result[0] if result[0] else '0.0'))
    set_statistic("rating_count", (result[1] if result[1] else 0))

    sandbox.flush_all()
    store.shutdown()

