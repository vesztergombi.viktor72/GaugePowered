# -*- coding: utf-8 -*-
import string
from gamestradamus import cli, units

#-------------------------------------------------------------------------------
allowed_characters = string.digits + string.letters + " "
def clean_name(name):
    cleaned_name = "".join([c for c in name if c in allowed_characters])
    return cleaned_name.strip()

#-------------------------------------------------------------------------------
class DuplicateFinder(object):

    #---------------------------------------------------------------------------
    def __init__(self, sandbox):
        self.sandbox = sandbox
        self.already_found_dupes = []
        self.false_positives = set()
        self._has_loaded_false_positives = False
        self.load_false_positives()

    #---------------------------------------------------------------------------
    def load_false_positives(self):
        if self._has_loaded_false_positives:
            return
        for unit in self.sandbox.recall(
                units.PossibleDuplicate,
                lambda dupe: dupe.resolved == True
            ):
            self.false_positives.add(tuple(sorted((unit.game_id_1, unit.game_id_2))))
        self._has_loaded_false_positives = True

    #---------------------------------------------------------------------------
    def find_duplicates(self, game, skip_canonical=False, skip_stat_sets=False):
        cleaned_name = clean_name(game.name)
        if not cleaned_name:
            return []
        name_parts = cleaned_name.split(" ")
        LIKE = "%%%s%%" % ("%".join(name_parts),)
        query = """
            SELECT id, app_id, name, canonical_game_id, stat_set_id
            FROM "Game"
            WHERE
                "is_dlc" = %s
                AND "app_id" <> %s
                AND "name" LIKE '%s'
            """ % ("true" if game.is_dlc else "false", game.app_id, LIKE)
        rows, cols = self.sandbox.store.db.fetch(query)

        duplicates = []
        for duplicate in rows:

            # Translate ids into ints
            id, app_id, name, canonical_game_id, stat_set_id = duplicate
            id = int(id)
            app_id = int(app_id)
            if canonical_game_id:
                canonical_game_id = int(canonical_game_id)
            if stat_set_id:
                stat_set_id = int(stat_set_id)
            duplicate = [id, app_id, name, canonical_game_id, stat_set_id]

            app_id_pair = set([app_id, game.app_id])

            # We've already reported on this duplicate
            if app_id_pair in self.already_found_dupes:
                continue

            # Allow us to record app ids that may appear to be dupes, but are not.
            if app_id_pair in self.false_positives:
                continue

            # If one of these is already a canonical game for the other one
            # then skip it.
            if id == game.canonical_game_id and skip_canonical:
                continue
            if canonical_game_id == game.id and skip_canonical:
                continue

            # Or if they are canonical to the same game:
            if canonical_game_id is not None and game.canonical_game_id is not None:
                if duplicate[3] == game.canonical_game_id and skip_canonical:
                    continue

            if stat_set_id is not None and stat_set_id == game.stat_set_id and skip_stat_sets:
                continue

            # Store them so we don't print this out again
            self.already_found_dupes.append(app_id_pair)

            # This is a legitimate duplicate
            duplicates.append(duplicate)
        return duplicates


    #-------------------------------------------------------------------------------
    def find_duplicate_false_positives(self):
        """
        A task to run and find potential duplicates that have not already been dealt
        with.
        """
        games = self.sandbox.recall(units.Game, lambda g: g.is_dlc == False)

        total_games = 0
        total_duplicates = 0

        games_with_duplicates = []

        for game in games:
            duplicates = self.find_duplicates(game, skip_canonical=True, skip_stat_sets=True)

            if len(duplicates) > 0:
                games_with_duplicates.append((game, duplicates))
                total_games += 1
                total_duplicates += len(duplicates)


        for count, game_with_duplicates in enumerate(games_with_duplicates):
            game, duplicates = game_with_duplicates
            for duplicate in duplicates:
                if not units.PossibleDuplicate.is_name_duplicate(self.sandbox, game.id, duplicate[0]):
                    dupe = units.PossibleDuplicate(
                        game_id_1=game.id,
                        game_id_2=duplicate[0]
                    )
                    self.sandbox.memorize(dupe)

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    store = cli.storage_manager(conflicts='repair')
    try:
        sandbox = store.new_sandbox()
        try:
            duplicate_finder = DuplicateFinder(sandbox)
            duplicate_finder.find_duplicate_false_positives()
        finally:
            sandbox.flush_all()
    finally:
        store.shutdown()

